package com.pl.bus;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.pl.process.MQProcessor;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;
import org.springframework.util.ClassUtils;
import org.springframework.util.StringUtils;


public class BusEventRegistrar
        implements ImportBeanDefinitionRegistrar {
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {

        Map<String, Object> componentScan = importingClassMetadata.getAnnotationAttributes(BusEventScan.class.getName(), false);


        Set<String> basePackages = new HashSet<>();

        for (String pkg : (String[]) componentScan.get("value")) {

            if (StringUtils.hasText(pkg)) {

                basePackages.add(pkg);
            }
        }

        for (String pkg : (String[]) componentScan.get("basePackages")) {

            if (StringUtils.hasText(pkg)) {

                basePackages.add(pkg);
            }
        }

        for (Class<?> clazz : (Class[]) componentScan.get("basePackageClasses")) {

            basePackages.add(ClassUtils.getPackageName(clazz));
        }


        if (basePackages.isEmpty()) {


            basePackages.add("com.cmsr.mos.common.model.event");


            basePackages.add(ClassUtils.getPackageName(importingClassMetadata.getClassName()));
        }


        MQProcessor.addPackagesToScan(basePackages.<String>toArray(new String[basePackages.size()]));
    }
}


/* Location:              E:\code\common\cmsr-common-mq-kafka\1.0.0-SNAPSHOT\cmsr-common-mq-kafka-1.0.0-20220121.074640-26.jar!\com\cmsr\common\mq\kafka\bus\BusEventRegistrar.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */