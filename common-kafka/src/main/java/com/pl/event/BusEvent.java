package com.pl.event;


public abstract class BusEvent extends AbstractMQEvent {
    public String toString() {
        return "BusEvent(super=" + super.toString() + ")";
    }


    protected BusEvent() {
    }


    public BusEvent(String originService) {
        /* 40 */
        super(originService);
    }
}


