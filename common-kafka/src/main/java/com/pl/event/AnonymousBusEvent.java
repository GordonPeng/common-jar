package com.pl.event;


public abstract class AnonymousBusEvent
        extends AbstractMQEvent {
    public String toString() {
        return "AnonymousBusEvent(super=" + super.toString() + ")";
    }


    protected AnonymousBusEvent() {
    }

    public AnonymousBusEvent(String originService) {
        super(originService);
    }
}


