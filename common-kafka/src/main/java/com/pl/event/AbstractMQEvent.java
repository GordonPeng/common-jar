package com.pl.event;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.pl.process.MQProcessor;


@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "type")
@JsonIgnoreProperties(ignoreUnknown = true)
public abstract class AbstractMQEvent {
    private final String id;
    private final long timestamp;
    private final String originService;

    public String toString() {
        return "AbstractMQEvent(id=" + getId() + ", timestamp=" + getTimestamp() + ", originService=" + getOriginService() + ")";
    }


    public String getId() {
        return this.id;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public String getOriginService() {
        return this.originService;
    }

    protected AbstractMQEvent() {
        this(null);
    }

    public AbstractMQEvent(String originService) {
        this.originService = originService;


        this.id = MQProcessor.MQ_ID + ":" + MQProcessor.nextEventId();
        this.timestamp = System.currentTimeMillis();
    }
}


