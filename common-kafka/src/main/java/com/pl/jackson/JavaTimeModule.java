package com.pl.jackson;

import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.PackageVersion;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;


public class JavaTimeModule
        extends SimpleModule {
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String HH_MM_SS = "HH:mm:ss";

    public JavaTimeModule() {

        super(PackageVersion.VERSION);

        addSerializer(LocalDateTime.class, (JsonSerializer) new LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        addSerializer(LocalDate.class, (JsonSerializer) new LocalDateSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        addSerializer(LocalTime.class, (JsonSerializer) new LocalTimeSerializer(DateTimeFormatter.ofPattern("HH:mm:ss")));

        addDeserializer(LocalDateTime.class, (JsonDeserializer) new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        addDeserializer(LocalDate.class, (JsonDeserializer) new LocalDateDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        addDeserializer(LocalTime.class, (JsonDeserializer) new LocalTimeDeserializer(DateTimeFormatter.ofPattern("HH:mm:ss")));
    }
}


