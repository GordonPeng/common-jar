package com.pl.process;

import com.pl.event.AbstractMQEvent;

public interface ProducerTopicSelector<T extends AbstractMQEvent> {
  String producerTopic(T paramT);
}


