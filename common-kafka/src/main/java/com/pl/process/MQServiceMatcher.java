package com.pl.process;

import com.pl.event.AbstractMQEvent;
import com.pl.event.AnonymousBusEvent;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.PathMatcher;


public class MQServiceMatcher
        implements ApplicationContextAware {
    private ApplicationContext context;
    private PathMatcher matcher;

    public void setApplicationContext(ApplicationContext context) throws BeansException {
        this.context = context;
    }

    public void setMatcher(PathMatcher matcher) {
        this.matcher = matcher;
    }

    public boolean isFromSelf(AbstractMQEvent event) {
        String originService = event.getOriginService();
        String serviceId = getServiceId();
        boolean match = this.matcher.match(originService, serviceId);
        if (match &&
                event instanceof AnonymousBusEvent) {


            return event.getId().startsWith(MQProcessor.MQ_ID);
        }

        return match;
    }

    public String getServiceId() {
        return this.context.getId();
    }
}


/* Location:              E:\code\common\cmsr-common-mq-kafka\1.0.0-SNAPSHOT\cmsr-common-mq-kafka-1.0.0-20220121.074640-26.jar!\com\cmsr\common\mq\kafka\process\MQServiceMatcher.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */