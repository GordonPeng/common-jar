package com.pl;

import java.beans.Introspector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Role;
import org.springframework.core.convert.converter.Converter;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ParseException;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.spel.standard.SpelExpression;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.integration.config.IntegrationConverter;
import org.springframework.integration.expression.SpelPropertyAccessorRegistrar;
import org.springframework.integration.json.JsonPropertyAccessor;


@Configuration
@Role(2)
public class SpelExpressionConverterConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public static SpelPropertyAccessorRegistrar spelPropertyAccessorRegistrar() {

        return (new SpelPropertyAccessorRegistrar())
                .add(Introspector.decapitalize(JsonPropertyAccessor.class.getSimpleName()), (PropertyAccessor) new JsonPropertyAccessor());
    }

    @Bean
    @ConfigurationPropertiesBinding
    @IntegrationConverter
    @ConditionalOnMissingBean
    public Converter<String, Expression> spelConverter() {

        return new SpelConverter();
    }


    public static class SpelConverter
            implements Converter<String, Expression> {
        private SpelExpressionParser parser = new SpelExpressionParser();

        @Autowired
        @Qualifier("integrationEvaluationContext")
        @Lazy
        private EvaluationContext evaluationContext;


        public Expression convert(String source) {
            try {

                Expression expression = this.parser.parseExpression(source);

                if (expression instanceof SpelExpression) {

                    ((SpelExpression) expression).setEvaluationContext(this.evaluationContext);
                }

                return expression;
            } catch (ParseException e) {

                throw new IllegalArgumentException(
                        String.format("Could not convert '%s' into a SpEL expression", new Object[]{source}), e);
            }
        }
    }
}


/* Location:              E:\code\common\cmsr-common-mq-kafka\1.0.0-SNAPSHOT\cmsr-common-mq-kafka-1.0.0-20220121.074640-26.jar!\com\cmsr\common\mq\kafka\SpelExpressionConverterConfiguration.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */