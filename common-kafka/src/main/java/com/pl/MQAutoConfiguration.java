package com.pl;

import com.pl.configuration.MQKafkaProperties;
import com.pl.process.MQProcessor;
import com.pl.process.MQServiceMatcher;
import org.apache.kafka.clients.admin.AdminClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;


@Configuration
@ComponentScan
@EnableConfigurationProperties({MQKafkaProperties.class})
@PropertySource({"classpath:/kafka.properties"})
public class MQAutoConfiguration {
    @Autowired
    private KafkaProperties kafkaProperties;
    public static final String MQ_PATH_MATCHER_NAME = "mqPathMatcher";
    public static final String MQ_SERVICE_MATCHER_NAME = "mqServiceMatcher";

    @ConditionalOnMissingBean(name = {"mqPathMatcher"})
    @Bean(name = {"mqPathMatcher"})
    public PathMatcher busPathMatcher() {
        return (PathMatcher) new AntPathMatcher(":");
    }

    @ConditionalOnMissingBean(name = {"mqServiceMatcher"})
    @Bean(name = {"mqServiceMatcher"})
    public MQServiceMatcher serviceMatcher(@Qualifier("mqPathMatcher") PathMatcher pathMatcher) {
        MQServiceMatcher serviceMatcher = new MQServiceMatcher();
        serviceMatcher.setMatcher(pathMatcher);
        return serviceMatcher;
    }

    @Bean
    public AdminClient kafkaAdminClient() {
        return AdminClient.create(this.kafkaProperties.buildAdminProperties());
    }


    @Bean(name = {"mqProcessor"})
    @ConditionalOnMissingBean(name = {"mqProcessor"})
    public MQProcessor mqProcessor() {
        return new MQProcessor();
    }
}


