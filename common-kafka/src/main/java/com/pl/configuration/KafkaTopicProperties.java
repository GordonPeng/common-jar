package com.pl.configuration;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;
import java.util.List;

import com.pl.event.AbstractMQEvent;
import org.springframework.expression.Expression;

public class KafkaTopicProperties {
    private String topicName;
    private Class<? extends AbstractMQEvent> messageClass;
    private boolean enableConsumer = false;
    private boolean enableProducer = false;
    private KafkaTopicProperties.Consumer consumer = new KafkaTopicProperties.Consumer();
    private KafkaTopicProperties.Producer producer = new KafkaTopicProperties.Producer();

    public KafkaTopicProperties() {
    }

    public String getTopicName() {
        return this.topicName;
    }

    public Class<? extends AbstractMQEvent> getMessageClass() {
        return this.messageClass;
    }

    public boolean isEnableConsumer() {
        return this.enableConsumer;
    }

    public boolean isEnableProducer() {
        return this.enableProducer;
    }

    public KafkaTopicProperties.Consumer getConsumer() {
        return this.consumer;
    }

    public KafkaTopicProperties.Producer getProducer() {
        return this.producer;
    }

    public void setTopicName(final String topicName) {
        this.topicName = topicName;
    }

    public void setMessageClass(final Class<? extends AbstractMQEvent> messageClass) {
        this.messageClass = messageClass;
    }

    public void setEnableConsumer(final boolean enableConsumer) {
        this.enableConsumer = enableConsumer;
    }

    public void setEnableProducer(final boolean enableProducer) {
        this.enableProducer = enableProducer;
    }

    public void setConsumer(final KafkaTopicProperties.Consumer consumer) {
        this.consumer = consumer;
    }

    public void setProducer(final KafkaTopicProperties.Producer producer) {
        this.producer = producer;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof KafkaTopicProperties)) {
            return false;
        } else {
            KafkaTopicProperties other = (KafkaTopicProperties) o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.isEnableConsumer() != other.isEnableConsumer()) {
                return false;
            } else if (this.isEnableProducer() != other.isEnableProducer()) {
                return false;
            } else {
                label64:
                {
                    Object this$topicName = this.getTopicName();
                    Object other$topicName = other.getTopicName();
                    if (this$topicName == null) {
                        if (other$topicName == null) {
                            break label64;
                        }
                    } else if (this$topicName.equals(other$topicName)) {
                        break label64;
                    }

                    return false;
                }

                label57:
                {
                    Object this$messageClass = this.getMessageClass();
                    Object other$messageClass = other.getMessageClass();
                    if (this$messageClass == null) {
                        if (other$messageClass == null) {
                            break label57;
                        }
                    } else if (this$messageClass.equals(other$messageClass)) {
                        break label57;
                    }

                    return false;
                }

                Object this$consumer = this.getConsumer();
                Object other$consumer = other.getConsumer();
                if (this$consumer == null) {
                    if (other$consumer != null) {
                        return false;
                    }
                } else if (!this$consumer.equals(other$consumer)) {
                    return false;
                }

                Object this$producer = this.getProducer();
                Object other$producer = other.getProducer();
                if (this$producer == null) {
                    if (other$producer != null) {
                        return false;
                    }
                } else if (!this$producer.equals(other$producer)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof KafkaTopicProperties;
    }

    public int hashCode() {
        int result = 1;
        result = result * 59 + (this.isEnableConsumer() ? 79 : 97);
        result = result * 59 + (this.isEnableProducer() ? 79 : 97);
        Object $topicName = this.getTopicName();
        result = result * 59 + ($topicName == null ? 43 : $topicName.hashCode());
        Object $messageClass = this.getMessageClass();
        result = result * 59 + ($messageClass == null ? 43 : $messageClass.hashCode());
        Object $consumer = this.getConsumer();
        result = result * 59 + ($consumer == null ? 43 : $consumer.hashCode());
        Object $producer = this.getProducer();
        result = result * 59 + ($producer == null ? 43 : $producer.hashCode());
        return result;
    }

    public String toString() {
        return "KafkaTopicProperties(topicName=" + this.getTopicName() + ", messageClass=" + this.getMessageClass() + ", enableConsumer=" + this.isEnableConsumer() + ", enableProducer=" + this.isEnableProducer() + ", consumer=" + this.getConsumer() + ", producer=" + this.getProducer() + ")";
    }

    static class ExpressionSerializer extends JsonSerializer<Expression> {
        ExpressionSerializer() {
        }

        public void serialize(Expression expression, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            if (expression != null) {
                jsonGenerator.writeString(expression.getExpressionString());
            }

        }
    }

    public static class Consumer {
        private boolean isAutoCommit = true;
        private boolean isConsumerGroup = true;
        private List<String> topicNames;
        private String consumerGroupId;

        public Consumer() {
        }

        public boolean isAutoCommit() {
            return this.isAutoCommit;
        }

        public boolean isConsumerGroup() {
            return this.isConsumerGroup;
        }

        public List<String> getTopicNames() {
            return this.topicNames;
        }

        public String getConsumerGroupId() {
            return this.consumerGroupId;
        }

        public void setAutoCommit(final boolean isAutoCommit) {
            this.isAutoCommit = isAutoCommit;
        }

        public void setConsumerGroup(final boolean isConsumerGroup) {
            this.isConsumerGroup = isConsumerGroup;
        }

        public void setTopicNames(final List<String> topicNames) {
            this.topicNames = topicNames;
        }

        public void setConsumerGroupId(final String consumerGroupId) {
            this.consumerGroupId = consumerGroupId;
        }

        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            } else if (!(o instanceof KafkaTopicProperties.Consumer)) {
                return false;
            } else {
                KafkaTopicProperties.Consumer other = (KafkaTopicProperties.Consumer) o;
                if (!other.canEqual(this)) {
                    return false;
                } else if (this.isAutoCommit() != other.isAutoCommit()) {
                    return false;
                } else if (this.isConsumerGroup() != other.isConsumerGroup()) {
                    return false;
                } else {
                    label40:
                    {
                        Object this$topicNames = this.getTopicNames();
                        Object other$topicNames = other.getTopicNames();
                        if (this$topicNames == null) {
                            if (other$topicNames == null) {
                                break label40;
                            }
                        } else if (this$topicNames.equals(other$topicNames)) {
                            break label40;
                        }

                        return false;
                    }

                    Object this$consumerGroupId = this.getConsumerGroupId();
                    Object other$consumerGroupId = other.getConsumerGroupId();
                    if (this$consumerGroupId == null) {
                        if (other$consumerGroupId != null) {
                            return false;
                        }
                    } else if (!this$consumerGroupId.equals(other$consumerGroupId)) {
                        return false;
                    }

                    return true;
                }
            }
        }

        protected boolean canEqual(final Object other) {
            return other instanceof KafkaTopicProperties.Consumer;
        }

        public int hashCode() {
            int result = 1;
            result = result * 59 + (this.isAutoCommit() ? 79 : 97);
            result = result * 59 + (this.isConsumerGroup() ? 79 : 97);
            Object $topicNames = this.getTopicNames();
            result = result * 59 + ($topicNames == null ? 43 : $topicNames.hashCode());
            Object $consumerGroupId = this.getConsumerGroupId();
            result = result * 59 + ($consumerGroupId == null ? 43 : $consumerGroupId.hashCode());
            return result;
        }

        public String toString() {
            return "KafkaTopicProperties.Consumer(isAutoCommit=" + this.isAutoCommit() + ", isConsumerGroup=" + this.isConsumerGroup() + ", topicNames=" + this.getTopicNames() + ", consumerGroupId=" + this.getConsumerGroupId() + ")";
        }
    }

    public static class Producer {
        @JsonSerialize(
                using = KafkaTopicProperties.ExpressionSerializer.class
        )
        private Expression partitionKeyExpression;
        private String producerTopicSelector;

        public Producer() {
        }

        public Expression getPartitionKeyExpression() {
            return this.partitionKeyExpression;
        }

        public String getProducerTopicSelector() {
            return this.producerTopicSelector;
        }

        public void setPartitionKeyExpression(final Expression partitionKeyExpression) {
            this.partitionKeyExpression = partitionKeyExpression;
        }

        public void setProducerTopicSelector(final String producerTopicSelector) {
            this.producerTopicSelector = producerTopicSelector;
        }

        public boolean equals(final Object o) {
            if (o == this) {
                return true;
            } else if (!(o instanceof KafkaTopicProperties.Producer)) {
                return false;
            } else {
                KafkaTopicProperties.Producer other = (KafkaTopicProperties.Producer) o;
                if (!other.canEqual(this)) {
                    return false;
                } else {
                    Object this$partitionKeyExpression = this.getPartitionKeyExpression();
                    Object other$partitionKeyExpression = other.getPartitionKeyExpression();
                    if (this$partitionKeyExpression == null) {
                        if (other$partitionKeyExpression != null) {
                            return false;
                        }
                    } else if (!this$partitionKeyExpression.equals(other$partitionKeyExpression)) {
                        return false;
                    }

                    Object this$producerTopicSelector = this.getProducerTopicSelector();
                    Object other$producerTopicSelector = other.getProducerTopicSelector();
                    if (this$producerTopicSelector == null) {
                        if (other$producerTopicSelector != null) {
                            return false;
                        }
                    } else if (!this$producerTopicSelector.equals(other$producerTopicSelector)) {
                        return false;
                    }

                    return true;
                }
            }
        }

        protected boolean canEqual(final Object other) {
            return other instanceof KafkaTopicProperties.Producer;
        }

        public int hashCode() {
            int result = 1;
            Object $partitionKeyExpression = this.getPartitionKeyExpression();
            result = result * 59 + ($partitionKeyExpression == null ? 43 : $partitionKeyExpression.hashCode());
            Object $producerTopicSelector = this.getProducerTopicSelector();
            result = result * 59 + ($producerTopicSelector == null ? 43 : $producerTopicSelector.hashCode());
            return result;
        }

        public String toString() {
            return "KafkaTopicProperties.Producer(partitionKeyExpression=" + this.getPartitionKeyExpression() + ", producerTopicSelector=" + this.getProducerTopicSelector() + ")";
        }
    }
}
