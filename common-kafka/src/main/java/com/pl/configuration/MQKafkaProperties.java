package com.pl.configuration;

import java.util.HashMap;
import java.util.Map;

import com.pl.configuration.KafkaTopicProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(
        prefix = "mos.mq.kafka"
)
public class MQKafkaProperties {
    private Map<String, KafkaTopicProperties> topic = new HashMap();
    private short replicationFactor = 2;
    private int partitionNum = 16;
    private boolean busEnabled = true;
    private String busTopic;
    private boolean anonymousBusEnable = true;
    private String anonymousBusTopic;

    public MQKafkaProperties() {
    }

    public Map<String, KafkaTopicProperties> getTopic() {
        return this.topic;
    }

    public short getReplicationFactor() {
        return this.replicationFactor;
    }

    public int getPartitionNum() {
        return this.partitionNum;
    }

    public boolean isBusEnabled() {
        return this.busEnabled;
    }

    public String getBusTopic() {
        return this.busTopic;
    }

    public boolean isAnonymousBusEnable() {
        return this.anonymousBusEnable;
    }

    public String getAnonymousBusTopic() {
        return this.anonymousBusTopic;
    }

    public void setTopic(final Map<String, KafkaTopicProperties> topic) {
        this.topic = topic;
    }

    public void setReplicationFactor(final short replicationFactor) {
        this.replicationFactor = replicationFactor;
    }

    public void setPartitionNum(final int partitionNum) {
        this.partitionNum = partitionNum;
    }

    public void setBusEnabled(final boolean busEnabled) {
        this.busEnabled = busEnabled;
    }

    public void setBusTopic(final String busTopic) {
        this.busTopic = busTopic;
    }

    public void setAnonymousBusEnable(final boolean anonymousBusEnable) {
        this.anonymousBusEnable = anonymousBusEnable;
    }

    public void setAnonymousBusTopic(final String anonymousBusTopic) {
        this.anonymousBusTopic = anonymousBusTopic;
    }

    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof MQKafkaProperties)) {
            return false;
        } else {
            MQKafkaProperties other = (MQKafkaProperties)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (this.getReplicationFactor() != other.getReplicationFactor()) {
                return false;
            } else if (this.getPartitionNum() != other.getPartitionNum()) {
                return false;
            } else if (this.isBusEnabled() != other.isBusEnabled()) {
                return false;
            } else if (this.isAnonymousBusEnable() != other.isAnonymousBusEnable()) {
                return false;
            } else {
                Object this$topic = this.getTopic();
                Object other$topic = other.getTopic();
                if (this$topic == null) {
                    if (other$topic != null) {
                        return false;
                    }
                } else if (!this$topic.equals(other$topic)) {
                    return false;
                }

                Object this$busTopic = this.getBusTopic();
                Object other$busTopic = other.getBusTopic();
                if (this$busTopic == null) {
                    if (other$busTopic != null) {
                        return false;
                    }
                } else if (!this$busTopic.equals(other$busTopic)) {
                    return false;
                }

                Object this$anonymousBusTopic = this.getAnonymousBusTopic();
                Object other$anonymousBusTopic = other.getAnonymousBusTopic();
                if (this$anonymousBusTopic == null) {
                    if (other$anonymousBusTopic != null) {
                        return false;
                    }
                } else if (!this$anonymousBusTopic.equals(other$anonymousBusTopic)) {
                    return false;
                }

                return true;
            }
        }
    }

    protected boolean canEqual(final Object other) {
        return other instanceof MQKafkaProperties;
    }

    public int hashCode() {
        int result = 1;
        result = result * 59 + this.getReplicationFactor();
        result = result * 59 + this.getPartitionNum();
        result = result * 59 + (this.isBusEnabled() ? 79 : 97);
        result = result * 59 + (this.isAnonymousBusEnable() ? 79 : 97);
        Object $topic = this.getTopic();
        result = result * 59 + ($topic == null ? 43 : $topic.hashCode());
        Object $busTopic = this.getBusTopic();
        result = result * 59 + ($busTopic == null ? 43 : $busTopic.hashCode());
        Object $anonymousBusTopic = this.getAnonymousBusTopic();
        result = result * 59 + ($anonymousBusTopic == null ? 43 : $anonymousBusTopic.hashCode());
        return result;
    }

    public String toString() {
        return "MQKafkaProperties(topic=" + this.getTopic() + ", replicationFactor=" + this.getReplicationFactor() + ", partitionNum=" + this.getPartitionNum() + ", busEnabled=" + this.isBusEnabled() + ", busTopic=" + this.getBusTopic() + ", anonymousBusEnable=" + this.isAnonymousBusEnable() + ", anonymousBusTopic=" + this.getAnonymousBusTopic() + ")";
    }
}
