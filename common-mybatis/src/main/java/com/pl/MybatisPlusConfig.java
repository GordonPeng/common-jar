package com.pl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.pl.cache.MybatisCacheService;
import com.pl.escape.MybatisLikeSqlInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan
public class MybatisPlusConfig {
    @Bean
    public MybatisCacheService mybatisCacheService() {

        return new MybatisCacheService();
    }

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(MybatisCacheService mybatisCacheService) {

        Long workerId = Long.valueOf(mybatisCacheService.nextMybatisWorkerId());


        Long dataCenterId = Long.valueOf(workerId.longValue() / 32L % 32L);

        IdWorker.initSequence(workerId.longValue() % 32L, dataCenterId.longValue());


        PaginationInnerInterceptor paginationInterceptor = new PaginationInnerInterceptor();


        paginationInterceptor.setOverflow(false);


        paginationInterceptor.setMaxLimit(Long.valueOf(1000L));


        paginationInterceptor.setOptimizeJoin(true);


        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

        interceptor.addInnerInterceptor((InnerInterceptor) paginationInterceptor);

        return interceptor;
    }


    @Bean
    public MybatisLikeSqlInterceptor escapeCharacterInterceptor() {

        return new MybatisLikeSqlInterceptor();
    }
}


/* Location:              E:\code\common\cmsr-common-mybatis\1.0.0-SNAPSHOT\cmsr-common-mybatis-1.0.0-SNAPSHOT.jar!\com\cmsr\common\mybatis\MybatisPlusConfig.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */