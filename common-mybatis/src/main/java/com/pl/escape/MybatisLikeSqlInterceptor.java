 package com.pl.escape;
 
 import java.util.HashMap;
 import java.util.HashSet;
 import java.util.Map;
 import java.util.Properties;
 import java.util.Set;
 import org.apache.commons.lang3.StringUtils;
 import org.apache.ibatis.executor.Executor;
 import org.apache.ibatis.mapping.BoundSql;
 import org.apache.ibatis.mapping.MappedStatement;
 import org.apache.ibatis.mapping.ParameterMapping;
 import org.apache.ibatis.plugin.Interceptor;
 import org.apache.ibatis.plugin.Intercepts;
 import org.apache.ibatis.plugin.Invocation;
 import org.apache.ibatis.plugin.Plugin;
 import org.apache.ibatis.plugin.Signature;
 import org.apache.ibatis.session.ResultHandler;
 import org.apache.ibatis.session.RowBounds;
 import org.slf4j.Logger;
 import org.slf4j.LoggerFactory;
 
 
 
 
 
 
 
 
 
 
 
 
 
 @Intercepts({@Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class})})
 public class MybatisLikeSqlInterceptor
   implements Interceptor
 {
/*  39 */   private static final Logger log = LoggerFactory.getLogger(MybatisLikeSqlInterceptor.class);
 
 
 
   
   private static final String SQL_LIKE = " like ";
 
 
 
   
   private static final String SQL_PLACEHOLDER = "?";
 
 
   
   private static final String SQL_PLACEHOLDER_REGEX = "\\?";
 
 
   
   public static final int INITIAL_CAPACITY = 4;
 
 
   
/*  61 */   private static Map<Class, AbstractLikeSqlConverter> converterMap = (Map)new HashMap<>(4);
   
   static {
/*  64 */     converterMap.put(Map.class, new MapLikeSqlConverter());
/*  65 */     converterMap.put(Object.class, new ObjectLikeSqlConverter());
   }
 
   
   public Object intercept(Invocation invocation) throws Throwable {
/*  70 */     Object[] args = invocation.getArgs();
/*  71 */     MappedStatement statement = (MappedStatement)args[0];
/*  72 */     Object parameterObject = args[1];
/*  73 */     BoundSql boundSql = statement.getBoundSql(parameterObject);
/*  74 */     String sql = boundSql.getSql();
/*  75 */     transferLikeSql(sql, parameterObject, boundSql);
/*  76 */     return invocation.proceed();
   }
 
   
   public Object plugin(Object target) {
/*  81 */     return Plugin.wrap(target, this);
   }
 
 
 
 
 
   
   public void setProperties(Properties arg0) {}
 
 
 
 
   
   private void transferLikeSql(String sql, Object parameterObject, BoundSql boundSql) {
     AbstractLikeSqlConverter<Object> converter;
/*  97 */     if (!isEscape(sql)) {
       return;
     }
/* 100 */     sql = sql.replaceAll(" {2}", " ");
     
/* 102 */     Set<String> fields = getKeyFields(sql, boundSql);
 
 
     
/* 106 */     if (parameterObject instanceof Map) {
/* 107 */       converter = converterMap.get(Map.class);
     } else {
/* 109 */       converter = converterMap.get(Object.class);
     } 
/* 111 */     converter.convert(sql, fields, parameterObject, boundSql);
   }
 
 
 
 
 
 
   
   private boolean isEscape(String sql) {
/* 121 */     return (hasLike(sql) && hasPlaceholder(sql));
   }
 
 
 
 
 
 
   
   private boolean hasLike(String str) {
/* 131 */     if (StringUtils.isBlank(str)) {
/* 132 */       return false;
     }
/* 134 */     return str.toLowerCase().contains(" like ");
   }
 
 
 
 
 
 
   
   private boolean hasPlaceholder(String str) {
/* 144 */     if (StringUtils.isBlank(str)) {
/* 145 */       return false;
     }
/* 147 */     return str.toLowerCase().contains("?");
   }
 
 
 
 
 
 
 
   
   private Set<String> getKeyFields(String sql, BoundSql boundSql) {
/* 158 */     String[] params = sql.split("\\?");
/* 159 */     Set<String> fields = new HashSet<>();
/* 160 */     for (int i = 0; i < params.length; i++) {
/* 161 */       if (hasLike(params[i])) {
/* 162 */         String field = ((ParameterMapping)boundSql.getParameterMappings().get(i)).getProperty();
/* 163 */         fields.add(field);
       } 
     } 
/* 166 */     return fields;
   }
 }


/* Location:              E:\code\common\cmsr-common-mybatis\1.0.0-SNAPSHOT\cmsr-common-mybatis-1.0.0-SNAPSHOT.jar!\com\cmsr\common\mybatis\escape\MybatisLikeSqlInterceptor.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */