/*    */ package com.pl.escape;
/*    */ 
/*    */ import org.apache.ibatis.mapping.BoundSql;
/*    */ import org.slf4j.Logger;
/*    */ import org.slf4j.LoggerFactory;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ObjectLikeSqlConverter
/*    */   extends AbstractLikeSqlConverter<Object>
/*    */ {
/* 30 */   private static final Logger log = LoggerFactory.getLogger(ObjectLikeSqlConverter.class);
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   public void transferWrapper(String field, Object parameter) {}
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   public void transferSelf(String field, Object parameter) {}
/*    */ 
/*    */ 
/*    */   
/*    */   public void transferSplice(String field, Object parameter, BoundSql boundSql) {
/* 45 */     resolveObj(field, parameter);
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-mybatis\1.0.0-SNAPSHOT\cmsr-common-mybatis-1.0.0-SNAPSHOT.jar!\com\cmsr\common\mybatis\escape\ObjectLikeSqlConverter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */