package com.pl.cache;

import com.cmsr.common.cache.redis.service.AbstractCacheService;


public class MybatisCacheService
        extends AbstractCacheService {
    private static final String KEY_MYBATIS_WORKER_ID = "MYBATIS_WORKER_ID";

    public long nextMybatisWorkerId() {
        return this.redisUtil.increament(this.redisKeys.getKey("MYBATIS_WORKER_ID")).longValue();
    }
}


