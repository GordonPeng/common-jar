package com.pl.handler.sensitive;


public class SensitiveFieldSM2Handler<T> extends SensitiveFieldHandler<T> {
    public SensitiveFieldSM2Handler(String secretKey) {
        super(IFieldCrypto.create(EFieldCryptoType.SM2, secretKey));
    }
}


