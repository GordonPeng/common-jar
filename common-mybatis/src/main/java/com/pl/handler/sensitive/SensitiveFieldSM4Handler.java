package com.pl.handler.sensitive;


public class SensitiveFieldSM4Handler
        extends SensitiveFieldHandler {
    public SensitiveFieldSM4Handler(String secretKey) {
        /* 12 */
        super(IFieldCrypto.create(EFieldCryptoType.SM4, secretKey));
    }
}


