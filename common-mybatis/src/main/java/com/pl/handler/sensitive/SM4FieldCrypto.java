package com.pl.handler.sensitive;

import cn.hutool.crypto.digest.MD5;
import cn.hutool.crypto.symmetric.SM4;


public class SM4FieldCrypto
        implements IFieldCrypto {
    private SM4 sm4;

    public SM4FieldCrypto(String privateKey) {
        this.sm4 = new SM4(MD5.create().digest(privateKey));
    }


    public String encrypt(String data) {
        return this.sm4.encryptHex(data);
    }


    public String decrypt(String data) {
        return this.sm4.decryptStr(data);
    }
}


