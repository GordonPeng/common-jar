package com.pl.handler.sensitive;

import cn.hutool.crypto.digest.MD5;

import java.math.BigInteger;

import com.pl.util.SM2Util;
import org.bouncycastle.math.ec.ECPoint;


public class SM2FieldCrypto
        implements IFieldCrypto {
    private String sm2PrivateKey;
    private ECPoint publicKey;

    public SM2FieldCrypto(String privateKey) {
        /* 26 */
        this.sm2PrivateKey = MD5.create().digestHex(privateKey);
        /* 27 */
        this.publicKey = SM2Util.getPublicKey(new BigInteger(this.sm2PrivateKey, 16));
    }


    public String encrypt(String data) {
        /* 32 */
        return SM2Util.encrypt(data, this.publicKey);
    }


    public String decrypt(String data) {
        /* 37 */
        return SM2Util.decrypt(data, new BigInteger(this.sm2PrivateKey, 16));
    }
}


/* Location:              E:\code\common\cmsr-common-mybatis\1.0.0-SNAPSHOT\cmsr-common-mybatis-1.0.0-SNAPSHOT.jar!\com\cmsr\common\mybatis\handler\sensitive\SM2FieldCrypto.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */