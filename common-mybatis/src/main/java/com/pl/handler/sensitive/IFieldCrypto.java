package com.pl.handler.sensitive;


public interface IFieldCrypto {
    static IFieldCrypto create(EFieldCryptoType cryptoType, String privateKey) {
        switch (cryptoType) {
            case SM2:
                return new SM2FieldCrypto(privateKey);
        }

        return new SM4FieldCrypto(privateKey);
    }

    String encrypt(String paramString);

    String decrypt(String paramString);
}

