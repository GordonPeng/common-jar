package com.pl.entity;

import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.util.Date;


public class BaseDo {
    @TableId(type = IdType.AUTO)
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setIsDelete(Boolean isDelete) {
        this.isDelete = isDelete;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof BaseDo)) return false;
        BaseDo other = (BaseDo) o;
        if (!other.canEqual(this)) return false;
        Object this$id = getId(), other$id = other.getId();
        if ((this$id == null) ? (other$id != null) : !this$id.equals(other$id)) return false;
        Object this$isDelete = getIsDelete(), other$isDelete = other.getIsDelete();
        if ((this$isDelete == null) ? (other$isDelete != null) : !this$isDelete.equals(other$isDelete)) return false;
        Object this$serial = getSerial(), other$serial = other.getSerial();
        if ((this$serial == null) ? (other$serial != null) : !this$serial.equals(other$serial)) return false;
        Object this$modifyBy = getModifyBy(), other$modifyBy = other.getModifyBy();
        if ((this$modifyBy == null) ? (other$modifyBy != null) : !this$modifyBy.equals(other$modifyBy)) return false;
        Object this$modifyTime = getModifyTime(), other$modifyTime = other.getModifyTime();
        if ((this$modifyTime == null) ? (other$modifyTime != null) : !this$modifyTime.equals(other$modifyTime))
            return false;
        Object this$createBy = getCreateBy(), other$createBy = other.getCreateBy();
        if ((this$createBy == null) ? (other$createBy != null) : !this$createBy.equals(other$createBy)) return false;
        Object this$createTime = getCreateTime(), other$createTime = other.getCreateTime();
        return !((this$createTime == null) ? (other$createTime != null) : !this$createTime.equals(other$createTime));
    }

    protected boolean canEqual(Object other) {
        return other instanceof BaseDo;
    }

    public int hashCode() {
        int PRIME = 59, result = 1;
        Object $id = getId();
        result = result * 59 + (($id == null) ? 43 : $id.hashCode());
        Object $isDelete = getIsDelete();
        result = result * 59 + (($isDelete == null) ? 43 : $isDelete.hashCode());
        Object $serial = getSerial();
        result = result * 59 + (($serial == null) ? 43 : $serial.hashCode());
        Object $modifyBy = getModifyBy();
        result = result * 59 + (($modifyBy == null) ? 43 : $modifyBy.hashCode());
        Object $modifyTime = getModifyTime();
        result = result * 59 + (($modifyTime == null) ? 43 : $modifyTime.hashCode());
        Object $createBy = getCreateBy();
        result = result * 59 + (($createBy == null) ? 43 : $createBy.hashCode());
        Object $createTime = getCreateTime();
        return result * 59 + (($createTime == null) ? 43 : $createTime.hashCode());
    }

    public String toString() {
        return "BaseDo(id=" + getId() + ", serial=" + getSerial() + ", modifyBy=" + getModifyBy() + ", modifyTime=" + getModifyTime() + ", createBy=" + getCreateBy() + ", createTime=" + getCreateTime() + ", isDelete=" + getIsDelete() + ")";
    }


    public Long getId() {
        return this.id;
    }


    private String serial = UUID.fastUUID().toString(true);
    private String modifyBy;

    public String getSerial() {
        return this.serial;
    }


    public String getModifyBy() {
        return this.modifyBy;
    }


    private Date modifyTime = new Date();
    private String createBy;

    public Date getModifyTime() {
        return this.modifyTime;
    }


    public String getCreateBy() {

        return this.createBy;
    }


    private Date createTime = new Date();

    public Date getCreateTime() {
        return this.createTime;
    }


    private Boolean isDelete = Boolean.valueOf(false);

    public Boolean getIsDelete() {
        return this.isDelete;
    }

}


