package com.pl.flush;

import java.util.Collection;

public interface DbFlushListener<T> {
  void dbFlushOver(Collection<T> paramCollection);
}


/* Location:              E:\code\common\cmsr-common-mybatis\1.0.0-SNAPSHOT\cmsr-common-mybatis-1.0.0-SNAPSHOT.jar!\com\cmsr\common\mybatis\flush\DbFlushListener.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */