/*     */ package com.pl.flush;
/*     */ 
/*     */ import cn.hutool.core.collection.CollectionUtil;
/*     */ import cn.hutool.core.date.DateUtil;
/*     */ import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/*     */ import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
/*     */ import com.google.common.collect.LinkedListMultimap;
/*     */ import com.google.common.collect.ListMultimap;
/*     */ import com.google.common.collect.Multimap;
/*     */ import com.google.common.collect.Multimaps;
/*     */ import java.util.Collection;
/*     */ import java.util.Date;
/*     */ import java.util.HashSet;
/*     */ import java.util.Set;
/*     */ import org.slf4j.Logger;
/*     */ import org.slf4j.LoggerFactory;
/*     */ 
/*     */ 
/*     */ public class DbFlushCacheUtil<T>
/*     */ {
/*  21 */   private static final Logger log = LoggerFactory.getLogger(DbFlushCacheUtil.class);
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*  27 */   private Multimap<String, T> saveFlushCacheMap = (Multimap<String, T>)Multimaps.synchronizedListMultimap((ListMultimap)LinkedListMultimap.create());
/*     */   
/*  29 */   private Multimap<String, T> updateFlushCacheMap = (Multimap<String, T>)Multimaps.synchronizedListMultimap((ListMultimap)LinkedListMultimap.create());
/*     */   
/*     */   private final ServiceImpl<? extends BaseMapper<T>, T> serviceImpl;
/*     */   
/*     */   public static final int BATCH_SIZE = 500;
/*     */   
/*     */   private DbFlushListener<T> listener;
/*     */   
/*     */   private int batchSize;
/*     */ 
/*     */   
/*     */   public DbFlushCacheUtil(ServiceImpl<? extends BaseMapper<T>, T> serviceImpl) {
/*  41 */     this(serviceImpl, null);
/*     */   }
/*     */   
/*     */   public DbFlushCacheUtil(ServiceImpl<? extends BaseMapper<T>, T> serviceImpl, DbFlushListener<T> listener) {
/*  45 */     this(serviceImpl, listener, 500);
/*     */   }
/*     */   
/*     */   public DbFlushCacheUtil(ServiceImpl<? extends BaseMapper<T>, T> serviceImpl, DbFlushListener<T> listener, int batchSize) {
/*  49 */     this.serviceImpl = serviceImpl;
/*  50 */     DbFlushScheduleService.addDbFlushCache(this);
/*  51 */     this.listener = listener;
/*  52 */     this.batchSize = batchSize;
/*     */   }
/*     */   
/*     */   public void save(T entity) {
/*  56 */     String secondKey = DateUtil.format(new Date(), "yyyyMMddHHmmss");
/*  57 */     this.saveFlushCacheMap.put(secondKey, entity);
/*     */   }
/*     */   
/*     */   public void saveBatch(Collection<T> entityList) {
/*  61 */     String secondKey = DateUtil.format(new Date(), "yyyyMMddHHmmss");
/*  62 */     this.saveFlushCacheMap.putAll(secondKey, entityList);
/*     */   }
/*     */   
/*     */   public void update(T entity) {
/*  66 */     String secondKey = DateUtil.format(new Date(), "yyyyMMddHHmmss");
/*  67 */     this.updateFlushCacheMap.put(secondKey, entity);
/*     */   }
/*     */   
/*     */   public void updateBatch(Collection<T> entityList) {
/*  71 */     String secondKey = DateUtil.format(new Date(), "yyyyMMddHHmmss");
/*  72 */     this.updateFlushCacheMap.putAll(secondKey, entityList);
/*     */   }
/*     */   
/*     */   public synchronized void flushCache() {
/*  76 */     String currentSecond = DateUtil.format(new Date(), "yyyyMMddHHmmss");
/*  77 */     Set<String> keys = new HashSet<>(this.saveFlushCacheMap.keys().elementSet());
/*  78 */     keys.addAll(this.updateFlushCacheMap.keys().elementSet());
/*     */     
/*  80 */     for (String key : keys) {
/*  81 */       if (currentSecond.equals(key)) {
/*     */         continue;
/*     */       }
/*     */       
/*     */       try {
/*  86 */         long start = System.currentTimeMillis();
/*     */         
/*  88 */         Collection<T> saveBatch = this.saveFlushCacheMap.get(key);
/*  89 */         if (CollectionUtil.isNotEmpty(saveBatch)) {
/*  90 */           this.serviceImpl.saveBatch(saveBatch, this.batchSize);
/*     */         }
/*     */         
/*  93 */         Collection<T> updateBatch = this.updateFlushCacheMap.get(key);
/*  94 */         if (CollectionUtil.isNotEmpty(updateBatch)) {
/*  95 */           this.serviceImpl.updateBatchById(updateBatch, this.batchSize);
/*     */         }
/*  97 */         log.info("flush cache {} entity in {}ms", Integer.valueOf(saveBatch.size() + updateBatch.size()), Long.valueOf(System.currentTimeMillis() - start));
/*  98 */         if (this.listener != null) {
/*  99 */           this.listener.dbFlushOver(saveBatch);
/* 100 */           this.listener.dbFlushOver(updateBatch);
/*     */         } 
/* 102 */       } catch (Exception e) {
/* 103 */         log.warn("flush cache to db error", e);
/*     */       } finally {
/*     */         
/* 106 */         this.saveFlushCacheMap.removeAll(key);
/* 107 */         this.updateFlushCacheMap.removeAll(key);
/*     */       } 
/*     */     } 
/*     */   }
/*     */ }


/* Location:              E:\code\common\cmsr-common-mybatis\1.0.0-SNAPSHOT\cmsr-common-mybatis-1.0.0-SNAPSHOT.jar!\com\cmsr\common\mybatis\flush\DbFlushCacheUtil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */