/*    */ package com.pl.limit;
/*    */ 
/*    */ import com.pl.limit.bucket.ArrayBucket;

import java.util.List;
/*    */ import java.util.concurrent.atomic.LongAdder;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FlowLimiter
/*    */ {
/*    */   private final ArrayBucket arrayBucket;
/*    */   private final long flowLimitPerSec;
/*    */   
/*    */   public FlowLimiter(long flowLimitPerSec) {
/* 39 */     this.arrayBucket = new ArrayBucket(10, 1000);
/* 40 */     this.flowLimitPerSec = flowLimitPerSec;
/*    */   }
/*    */ 
/*    */   
/*    */   private long getCurrentBucketFlowCount() {
/* 45 */     this.arrayBucket.currentWindow();
/* 46 */     long count = 0L;
/*    */     
/* 48 */     List<LongAdder> list = this.arrayBucket.values();
/* 49 */     for (LongAdder window : list) {
/* 50 */       count += window.sum();
/*    */     }
/* 52 */     return count;
/*    */   }
/*    */   
/*    */   public boolean grabToken() {
/* 56 */     if (getCurrentBucketFlowCount() + 1L <= this.flowLimitPerSec) {
/* 57 */       this.arrayBucket.currentWindow().value().add(1L);
/* 58 */       return true;
/*    */     } 
/* 60 */     return false;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\limit\FlowLimiter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */