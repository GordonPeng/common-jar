/*    */ package com.pl.limit;

import com.pl.util.Constant;

/*    */
/*    */
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class FlowLimiterManager
/*    */ {
/*    */   public static boolean grabToken(String limiterId, long flowLimitPerSec) {
/* 33 */     if (limiterId == null || "".equals(limiterId)) {
/* 34 */       throw new IllegalArgumentException("LimiterId cannot be empty.");
/*    */     }
/* 36 */     FlowLimiter limiter = (FlowLimiter) Constant.GLOBAL_FLOW_LIMITER_MAP.get(limiterId);
/* 37 */     if (null == limiter) {
/* 38 */       Constant.GLOBAL_FLOW_LIMITER_MAP.put(limiterId, new FlowLimiter(flowLimitPerSec));
/* 39 */       return true;
/*    */     } 
/* 41 */     return limiter.grabToken();
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\limit\FlowLimiterManager.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */