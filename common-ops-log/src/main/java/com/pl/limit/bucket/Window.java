/*    */ package com.pl.limit.bucket;
/*    */ 
/*    */ import java.util.concurrent.atomic.LongAdder;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class Window
/*    */ {
/*    */   private final long windowLengthInMs;
/*    */   private long windowStart;
/*    */   private LongAdder value;
/*    */   
/*    */   public Window(long windowLengthInMs, long windowStart, LongAdder value) {
/* 44 */     this.windowLengthInMs = windowLengthInMs;
/* 45 */     this.windowStart = windowStart;
/* 46 */     this.value = value;
/*    */   }
/*    */   
/*    */   public long windowLength() {
/* 50 */     return this.windowLengthInMs;
/*    */   }
/*    */   
/*    */   public long windowStart() {
/* 54 */     return this.windowStart;
/*    */   }
/*    */   
/*    */   public LongAdder value() {
/* 58 */     return this.value;
/*    */   }
/*    */   
/*    */   public void setValue(LongAdder value) {
/* 62 */     this.value = value;
/*    */   }
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   public Window resetTo(long startTime) {
/* 69 */     this.windowStart = startTime;
/* 70 */     return this;
/*    */   }
/*    */ 
/*    */ 
/*    */ 
/*    */   
/*    */   public boolean isTimeInWindow(long timeMillis) {
/* 77 */     return (this.windowStart <= timeMillis && timeMillis < this.windowStart + this.windowLengthInMs);
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\limit\bucket\Window.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */