/*     */ package com.pl;
/*     */ 
/*     */ import cn.hutool.core.exceptions.ExceptionUtil;
/*     */ import com.pl.configuration.OpsProperties;
import com.pl.event.MethodExceptionModel;
import com.pl.limit.FlowLimiterManager;
import com.pl.log.InvokeLogUtil;
import com.pl.log.MethodInvokeLog;
import com.pl.util.Constant;
import com.pl.util.HttpRequestUtil;
import com.pl.util.OpsConfig;
import com.pl.util.TimeUtil;
import io.micrometer.core.instrument.Metrics;
/*     */ import io.micrometer.core.instrument.Timer;
/*     */ import java.time.Duration;
/*     */ import javax.servlet.http.HttpServletRequest;
/*     */ import org.aspectj.lang.ProceedingJoinPoint;
/*     */ import org.aspectj.lang.annotation.Around;
/*     */ import org.aspectj.lang.annotation.Aspect;
/*     */ import org.aspectj.lang.annotation.Pointcut;
/*     */ import org.slf4j.Logger;
/*     */ import org.slf4j.LoggerFactory;
/*     */ import org.springframework.core.annotation.Order;
/*     */ import org.springframework.stereotype.Component;
/*     */ import org.springframework.web.context.request.RequestAttributes;
/*     */ import org.springframework.web.context.request.RequestContextHolder;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @Order(2)
/*     */ @Aspect
/*     */ @Component
/*     */ public class OpsAop
/*     */ {
/*  41 */   private final Logger log = LoggerFactory.getLogger(OpsAop.class);
/*     */ 
/*     */   
/*     */   private OpsProperties opsProperties;
/*     */ 
/*     */   
/*     */   public static final String SEPARATOR = ":";
/*     */ 
/*     */ 
/*     */   
/*     */   @Pointcut("@annotation(com.pl.annotation.MethodInvokeStatsOps)")
/*     */   public void methodInvokeStats() {}
/*     */ 
/*     */   
/*     */   @Pointcut("@within(org.springframework.web.bind.annotation.RestController)")
/*     */   public void restControllerStats() {}
/*     */ 
/*     */   
/*     */   @Around("methodInvokeStats() || restControllerStats()")
/*     */   public Object invokeAround(ProceedingJoinPoint point) throws Throwable {
/*  61 */     long start = System.currentTimeMillis();
/*  62 */     String processMinute = TimeUtil.currentTimeMinuteStr();
/*  63 */     StringBuilder methodBuilder = new StringBuilder();
/*  64 */     String className = point.getTarget().getClass().getName();
/*  65 */     String methodName = point.getSignature().getName();
/*  66 */     methodBuilder.append(className)
/*  67 */       .append(":")
/*  68 */       .append(methodName);
/*     */     
/*  70 */     String methodSignature = methodBuilder.toString();
/*  71 */     Constant.methodSignatureSet.add(methodSignature);
/*     */ 
/*     */     
/*  74 */     Timer timer = Metrics.timer("method.invoke.perf", new String[] { "method.tag.signature", methodSignature, "method.tag.timestamp", processMinute });
/*     */     
/*     */     try {
/*  77 */       Object result = point.proceed();
/*  78 */       return result;
/*  79 */     } catch (Throwable e) {
/*     */ 
/*     */       
/*  82 */       Metrics.counter("method.invoke.count.fail", new String[] { "method.tag.signature", methodSignature, "method.tag.timestamp", processMinute
/*  83 */           }).increment();
/*     */       
/*  85 */       if (FlowLimiterManager.grabToken("log.exception.limiter", OpsConfig.getErrorLogLimitPerSec())) {
/*     */         try {
/*  87 */           StackTraceElement[] stList = Thread.currentThread().getStackTrace();
/*  88 */           if (stList.length > 0) {
/*  89 */             StackTraceElement st = stList[0];
/*  90 */             String traceId = getTraceIdString();
/*     */ 
/*     */ 
/*     */             
/*  94 */             MethodExceptionModel exceptionModel = new MethodExceptionModel(st.getClassName(), st.getMethodName(), st.getLineNumber(), e.getMessage(), ExceptionUtil.stacktraceToString(e), HttpRequestUtil.getRequestHeader(), HttpRequestUtil.getRequestParam(point), traceId);
/*  95 */             Constant.exceptionMap.put(processMinute, exceptionModel);
/*     */           } else {
/*  97 */             this.log.error(" StackTrace empty");
/*     */           } 
/*  99 */         } catch (Exception es) {
/* 100 */           this.log.error("errorLog ", es);
/*     */         } 
/*     */       }
/* 103 */       throw e;
/*     */     } finally {
/* 105 */       long duration = System.currentTimeMillis() - start;
/* 106 */       timer.record(Duration.ofMillis(duration));
/*     */     } 
/*     */   }
/*     */   
/*     */   private String getTraceIdString() {
/* 111 */     RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
/*     */     
/* 113 */     HttpServletRequest request = null;
/* 114 */     if (requestAttributes != null) {
/* 115 */       Object requestObj = requestAttributes.resolveReference("request");
/* 116 */       if (requestObj instanceof HttpServletRequest) {
/* 117 */         request = (HttpServletRequest)requestObj;
/*     */       }
/*     */     } 
/*     */     
/* 121 */     MethodInvokeLog methodInvokeLog = InvokeLogUtil.getMethodInvokeLog();
/* 122 */     String traceId = null;
/* 123 */     if (methodInvokeLog != null) {
/* 124 */       traceId = methodInvokeLog.getTraceId();
/*     */     }
/* 126 */     else if (request != null) {
/*     */       
/* 128 */       traceId = request.getHeader("trace-id");
/*     */     } 
/*     */     
/* 131 */     return traceId;
/*     */   }
/*     */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\OpsAop.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */