/*     */ package com.pl.event;
/*     */ 
/*     */ import cn.hutool.core.date.DateUnit;
/*     */ import cn.hutool.core.date.DateUtil;
/*     */ import java.util.Date;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class ExtApiLogOpsEvent
/*     */   extends AbstractOpsEvent
/*     */ {
/*     */   private String apiDesc;
/*     */   private String apiId;
/*     */   private String uri;
/*     */   private String requestParams;
/*     */   private String requestHeader;
/*     */   private String responseBody;
/*     */   private String responseCode;
/*     */   private String errorMsg;
/*     */   private int invokeResult;
/*     */   private Date invokeTime;
/*     */   private int duration;
/*     */   private String thirdPlatform;
/*     */   
/*     */   private ExtApiLogOpsEvent() {}
/*     */   
/*     */   public ExtApiLogOpsEvent(String project, String application, String apiId, String uri, String requestHeader, String requestParams, String responseBody, String responseCode, Date invokeTime, boolean isSuccess) {
/*  97 */     super(project, application);
/*     */     
/*  99 */     assert invokeTime != null;
/*     */     
/* 101 */     this.apiId = apiId;
/* 102 */     this.uri = uri;
/* 103 */     this.requestHeader = requestHeader;
/* 104 */     this.requestParams = requestParams;
/* 105 */     this.responseBody = responseBody;
/* 106 */     this.responseCode = responseCode;
/* 107 */     this.invokeTime = invokeTime;
/* 108 */     this.invokeResult = isSuccess ? 0 : 1;
/*     */     
/* 110 */     this.duration = (int)DateUtil.between(invokeTime, new Date(), DateUnit.MS);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public ExtApiLogOpsEvent(String project, String application, String apiId, String uri, String requestHeader, String requestParams, Date invokeTime, String errorMsg) {
/* 126 */     super(project, application);
/*     */     
/* 128 */     assert invokeTime != null;
/*     */     
/* 130 */     this.apiId = apiId;
/* 131 */     this.uri = uri;
/* 132 */     this.requestHeader = requestHeader;
/* 133 */     this.requestParams = requestParams;
/* 134 */     this.invokeTime = invokeTime;
/* 135 */     this.errorMsg = errorMsg;
/* 136 */     this.invokeResult = 1;
/*     */     
/* 138 */     this.duration = (int)DateUtil.between(invokeTime, new Date(), DateUnit.MS);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public ExtApiLogOpsEvent(String project, String application, String apiId, String thirdPlatform, String apiDesc, String uri, String requestHeader, String requestParams, String responseBody, String responseCode, Date invokeTime, boolean isSuccess, String errorMsg) {
/* 160 */     super(project, application);
/*     */     
/* 162 */     assert invokeTime != null;
/*     */     
/* 164 */     this.apiId = apiId;
/* 165 */     this.thirdPlatform = thirdPlatform;
/* 166 */     this.apiDesc = apiDesc;
/* 167 */     this.uri = uri;
/* 168 */     this.requestHeader = requestHeader;
/* 169 */     this.requestParams = requestParams;
/* 170 */     this.responseBody = responseBody;
/* 171 */     this.responseCode = responseCode;
/* 172 */     this.invokeTime = invokeTime;
/* 173 */     this.invokeResult = isSuccess ? 0 : 1;
/* 174 */     this.errorMsg = errorMsg;
/*     */     
/* 176 */     this.duration = (int)DateUtil.between(invokeTime, new Date(), DateUnit.MS);
/*     */   }
/*     */   
/*     */   public String getApiDesc() {
/* 180 */     return this.apiDesc;
/*     */   }
/*     */   
/*     */   public void setApiDesc(String apiDesc) {
/* 184 */     this.apiDesc = apiDesc;
/*     */   }
/*     */   
/*     */   public String getApiId() {
/* 188 */     return this.apiId;
/*     */   }
/*     */   
/*     */   public void setApiId(String apiId) {
/* 192 */     this.apiId = apiId;
/*     */   }
/*     */   
/*     */   public String getUri() {
/* 196 */     return this.uri;
/*     */   }
/*     */   
/*     */   public void setUri(String uri) {
/* 200 */     this.uri = uri;
/*     */   }
/*     */   
/*     */   public String getRequestParams() {
/* 204 */     return this.requestParams;
/*     */   }
/*     */   
/*     */   public void setRequestParams(String requestParams) {
/* 208 */     this.requestParams = requestParams;
/*     */   }
/*     */   
/*     */   public String getRequestHeader() {
/* 212 */     return this.requestHeader;
/*     */   }
/*     */   
/*     */   public void setRequestHeader(String requestHeader) {
/* 216 */     this.requestHeader = requestHeader;
/*     */   }
/*     */   
/*     */   public String getResponseBody() {
/* 220 */     return this.responseBody;
/*     */   }
/*     */   
/*     */   public void setResponseBody(String responseBody) {
/* 224 */     this.responseBody = responseBody;
/*     */   }
/*     */   
/*     */   public String getResponseCode() {
/* 228 */     return this.responseCode;
/*     */   }
/*     */   
/*     */   public void setResponseCode(String responseCode) {
/* 232 */     this.responseCode = responseCode;
/*     */   }
/*     */   
/*     */   public String getErrorMsg() {
/* 236 */     return this.errorMsg;
/*     */   }
/*     */   
/*     */   public void setErrorMsg(String errorMsg) {
/* 240 */     this.errorMsg = errorMsg;
/*     */   }
/*     */   
/*     */   public int getInvokeResult() {
/* 244 */     return this.invokeResult;
/*     */   }
/*     */   
/*     */   public void setInvokeResult(int invokeResult) {
/* 248 */     this.invokeResult = invokeResult;
/*     */   }
/*     */   
/*     */   public Date getInvokeTime() {
/* 252 */     return this.invokeTime;
/*     */   }
/*     */   
/*     */   public void setInvokeTime(Date invokeTime) {
/* 256 */     this.invokeTime = invokeTime;
/*     */   }
/*     */   
/*     */   public int getDuration() {
/* 260 */     return this.duration;
/*     */   }
/*     */   
/*     */   public void setDuration(int duration) {
/* 264 */     this.duration = duration;
/*     */   }
/*     */   
/*     */   public String getThirdPlatform() {
/* 268 */     return this.thirdPlatform;
/*     */   }
/*     */   
/*     */   public void setThirdPlatform(String thirdPlatform) {
/* 272 */     this.thirdPlatform = thirdPlatform;
/*     */   }
/*     */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\event\ExtApiLogOpsEvent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */