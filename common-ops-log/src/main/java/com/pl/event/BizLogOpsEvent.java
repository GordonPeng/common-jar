/*    */ package com.pl.event;
/*    */ 
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class BizLogOpsEvent
/*    */   extends AbstractOpsEvent
/*    */ {
/*    */   private String serviceIp;
/*    */   private Integer servicePort;
/*    */   private List<BizLogOpsModel> bizLogOpsModelList;
/*    */   
/*    */   private BizLogOpsEvent() {}
/*    */   
/*    */   public BizLogOpsEvent(String project, String application, List<BizLogOpsModel> bizLogOpsModelList) {
/* 48 */     super(project, application);
/* 49 */     this.bizLogOpsModelList = bizLogOpsModelList;
/*    */   }
/*    */   
/*    */   public BizLogOpsEvent(String serviceIp, Integer servicePort, String project, String application, List<BizLogOpsModel> bizLogOpsModelList) {
/* 53 */     super(project, application);
/* 54 */     this.servicePort = servicePort;
/* 55 */     this.serviceIp = serviceIp;
/* 56 */     this.bizLogOpsModelList = bizLogOpsModelList;
/*    */   }
/*    */   
/*    */   public List<BizLogOpsModel> getBizLogOpsModelList() {
/* 60 */     return this.bizLogOpsModelList;
/*    */   }
/*    */   
/*    */   public void setBizLogOpsModelList(List<BizLogOpsModel> bizLogOpsModelList) {
/* 64 */     this.bizLogOpsModelList = bizLogOpsModelList;
/*    */   }
/*    */   
/*    */   public String getServiceIp() {
/* 68 */     return this.serviceIp;
/*    */   }
/*    */   
/*    */   public Integer getServicePort() {
/* 72 */     return this.servicePort;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\event\BizLogOpsEvent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */