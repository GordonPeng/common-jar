/*    */ package com.pl.event;
/*    */ 
/*    */ import java.util.Date;
/*    */ import java.util.List;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class MethodOpsEvent
/*    */   extends AbstractOpsEvent
/*    */ {
/*    */   private String serviceIp;
/*    */   private Integer servicePort;
/*    */   private Date logTime;
/*    */   private List<MethodOpsModel> methodOpsModelList;
/*    */   private List<MethodExceptionModel> methodExceptionModelList;
/*    */   
/*    */   private MethodOpsEvent() {}
/*    */   
/*    */   public MethodOpsEvent(String project, String serviceIp, Integer servicePort, String application, List<MethodOpsModel> methodOpsModelList, List<MethodExceptionModel> methodExceptionModelList) {
/* 31 */     super(project, application);
/*    */     
/* 33 */     this.serviceIp = serviceIp;
/* 34 */     this.servicePort = servicePort;
/* 35 */     this.methodOpsModelList = methodOpsModelList;
/*    */     
/* 37 */     this.methodExceptionModelList = methodExceptionModelList;
/*    */     
/* 39 */     this.logTime = new Date();
/*    */   }
/*    */   
/*    */   public String getServiceIp() {
/* 43 */     return this.serviceIp;
/*    */   }
/*    */   
/*    */   public void setServiceIp(String serviceIp) {
/* 47 */     this.serviceIp = serviceIp;
/*    */   }
/*    */   
/*    */   public Integer getServicePort() {
/* 51 */     return this.servicePort;
/*    */   }
/*    */   
/*    */   public void setServicePort(Integer servicePort) {
/* 55 */     this.servicePort = servicePort;
/*    */   }
/*    */   
/*    */   public List<MethodOpsModel> getMethodOpsModelList() {
/* 59 */     return this.methodOpsModelList;
/*    */   }
/*    */   
/*    */   public void setMethodOpsModelList(List<MethodOpsModel> methodOpsModelList) {
/* 63 */     this.methodOpsModelList = methodOpsModelList;
/*    */   }
/*    */   
/*    */   public Date getLogTime() {
/* 67 */     return this.logTime;
/*    */   }
/*    */   
/*    */   public void setLogTime(Date logTime) {
/* 71 */     this.logTime = logTime;
/*    */   }
/*    */   
/*    */   public List<MethodExceptionModel> getMethodExceptionModelList() {
/* 75 */     return this.methodExceptionModelList;
/*    */   }
/*    */   
/*    */   public void setMethodExceptionModelList(List<MethodExceptionModel> methodExceptionModelList) {
/* 79 */     this.methodExceptionModelList = methodExceptionModelList;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\event\MethodOpsEvent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */