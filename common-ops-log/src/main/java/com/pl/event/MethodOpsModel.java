/*     */ package com.pl.event;
/*     */ 
/*     */ import java.util.Date;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class MethodOpsModel
/*     */ {
/*     */   private String className;
/*     */   private String methodName;
/*     */   private Date minute;
/*     */   private long count;
/*     */   private long failCount;
/*     */   private double mean;
/*     */   private double max;
/*     */   
/*     */   private MethodOpsModel() {}
/*     */   
/*     */   public MethodOpsModel(String className, String methodName, Date minute, long count, long failCount, double mean, double max) {
/*  46 */     this.className = className;
/*  47 */     this.methodName = methodName;
/*  48 */     this.minute = minute;
/*  49 */     this.count = count;
/*  50 */     this.failCount = failCount;
/*  51 */     this.mean = mean;
/*  52 */     this.max = max;
/*     */   }
/*     */   
/*     */   public String getClassName() {
/*  56 */     return this.className;
/*     */   }
/*     */   
/*     */   public void setClassName(String className) {
/*  60 */     this.className = className;
/*     */   }
/*     */   
/*     */   public String getMethodName() {
/*  64 */     return this.methodName;
/*     */   }
/*     */   
/*     */   public void setMethodName(String methodName) {
/*  68 */     this.methodName = methodName;
/*     */   }
/*     */   
/*     */   public Date getMinute() {
/*  72 */     return this.minute;
/*     */   }
/*     */   
/*     */   public void setMinute(Date minute) {
/*  76 */     this.minute = minute;
/*     */   }
/*     */   
/*     */   public long getCount() {
/*  80 */     return this.count;
/*     */   }
/*     */   
/*     */   public void setCount(long count) {
/*  84 */     this.count = count;
/*     */   }
/*     */   
/*     */   public long getFailCount() {
/*  88 */     return this.failCount;
/*     */   }
/*     */   
/*     */   public void setFailCount(long failCount) {
/*  92 */     this.failCount = failCount;
/*     */   }
/*     */   
/*     */   public double getMean() {
/*  96 */     return this.mean;
/*     */   }
/*     */   
/*     */   public void setMean(double mean) {
/* 100 */     this.mean = mean;
/*     */   }
/*     */   
/*     */   public double getMax() {
/* 104 */     return this.max;
/*     */   }
/*     */   
/*     */   public void setMax(double max) {
/* 108 */     this.max = max;
/*     */   }
/*     */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\event\MethodOpsModel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */