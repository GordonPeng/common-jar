package com.pl.event;

import java.util.Date;


public class MethodInvokeLogOpsEvent extends AbstractOpsEvent {
    private String traceId;
    private int level;
    private String parentId;
    private String methodDesc;
    private String className;
    private String methodName;
    private String uri;
    private String requestHeader;
    private String requestParams;
    private String responseBody;
    private String errorMsg;
    private int invokeResult;
    private Date invokeTime;
    private int duration;
    private String methodType;

    public MethodInvokeLogOpsEvent() {
    }

    public MethodInvokeLogOpsEvent(String project, String application, String traceId, String className, String methodName, int level, String parentId, String requestParams, String responseBody, String errorMsg, int invokeResult, Date invokeTime, long duration, String uri, String methodDesc) {
        /*  97 */
        this(project, application, traceId, className, methodName, level, parentId, requestParams, responseBody, errorMsg, invokeResult, invokeTime, duration, uri, methodDesc, null);
    }


    public MethodInvokeLogOpsEvent(String project, String application, String traceId, String className, String methodName, int level, String parentId, String requestParams, String responseBody, String errorMsg, int invokeResult, Date invokeTime, long duration, String uri, String methodDesc, String methodType) {
        /* 106 */
        this(project, application, traceId, className, methodName, level, parentId, requestParams, responseBody, errorMsg, invokeResult, invokeTime, duration, uri, methodDesc, methodType, null);
    }


    public MethodInvokeLogOpsEvent(String project, String application, String traceId, String className, String methodName, int level, String parentId, String requestParams, String responseBody, String errorMsg, int invokeResult, Date invokeTime, long duration, String uri, String methodDesc, String methodType, String requestHeader) {
        /* 115 */
        super(project, application);
        /* 116 */
        this.traceId = traceId;

        /* 118 */
        this.className = className;
        /* 119 */
        this.methodName = methodName;

        /* 121 */
        this.level = level;
        /* 122 */
        this.parentId = parentId;
        /* 123 */
        this.requestParams = requestParams;
        /* 124 */
        this.responseBody = responseBody;
        /* 125 */
        this.errorMsg = errorMsg;
        /* 126 */
        this.invokeResult = invokeResult;
        /* 127 */
        this.invokeTime = invokeTime;
        /* 128 */
        this.duration = (int) duration;

        /* 130 */
        this.uri = uri;
        /* 131 */
        this.methodDesc = methodDesc;
        /* 132 */
        this.methodType = methodType;
        /* 133 */
        this.requestHeader = requestHeader;
    }

    public String getTraceId() {
        /* 137 */
        return this.traceId;
    }

    public void setTraceId(String traceId) {
        /* 141 */
        this.traceId = traceId;
    }

    public int getLevel() {
        /* 145 */
        return this.level;
    }

    public void setLevel(int level) {
        /* 149 */
        this.level = level;
    }

    public String getParentId() {
        /* 153 */
        return this.parentId;
    }

    public void setParentId(String parentId) {
        /* 157 */
        this.parentId = parentId;
    }

    public String getRequestHeader() {
        /* 161 */
        return this.requestHeader;
    }

    public void setRequestHeader(String requestHeader) {
        /* 165 */
        this.requestHeader = requestHeader;
    }

    public String getRequestParams() {
        /* 169 */
        return this.requestParams;
    }

    public void setRequestParams(String requestParams) {
        /* 173 */
        this.requestParams = requestParams;
    }

    public String getResponseBody() {
        /* 177 */
        return this.responseBody;
    }

    public void setResponseBody(String responseBody) {
        /* 181 */
        this.responseBody = responseBody;
    }

    public String getErrorMsg() {
        /* 185 */
        return this.errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        /* 189 */
        this.errorMsg = errorMsg;
    }

    public int getInvokeResult() {
        /* 193 */
        return this.invokeResult;
    }

    public void setInvokeResult(int invokeResult) {
        /* 197 */
        this.invokeResult = invokeResult;
    }

    public Date getInvokeTime() {
        /* 201 */
        return this.invokeTime;
    }

    public void setInvokeTime(Date invokeTime) {
        /* 205 */
        this.invokeTime = invokeTime;
    }

    public int getDuration() {
        /* 209 */
        return this.duration;
    }

    public void setDuration(int duration) {
        /* 213 */
        this.duration = duration;
    }

    public String getMethodDesc() {
        /* 217 */
        return this.methodDesc;
    }

    public void setMethodDesc(String methodDesc) {
        /* 221 */
        this.methodDesc = methodDesc;
    }

    public String getUri() {
        /* 225 */
        return this.uri;
    }

    public void setUri(String uri) {
        /* 229 */
        this.uri = uri;
    }

    public String getClassName() {
        /* 233 */
        return this.className;
    }

    public void setClassName(String className) {
        /* 237 */
        this.className = className;
    }

    public String getMethodName() {
        /* 241 */
        return this.methodName;
    }

    public void setMethodName(String methodName) {
        /* 245 */
        this.methodName = methodName;
    }

    public String getMethodType() {
        /* 249 */
        return this.methodType;
    }

    public void setMethodType(String methodType) {
        /* 253 */
        this.methodType = methodType;
    }
}


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\event\MethodInvokeLogOpsEvent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */