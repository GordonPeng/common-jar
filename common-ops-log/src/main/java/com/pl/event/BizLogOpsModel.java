/*     */ package com.pl.event;
/*     */ 
/*     */ import java.util.Date;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class BizLogOpsModel
/*     */ {
/*     */   private String className;
/*     */   private String methodName;
/*     */   private int lineNumber;
/*     */   private String logType;
/*     */   private String logSubType;
/*     */   private String module;
/*     */   private String msgContent;
/*     */   private String traceId;
/*     */   private Date logTime;
/*     */   
/*     */   public BizLogOpsModel() {}
/*     */   
/*     */   public BizLogOpsModel(String className, String methodName, int lineNumber, String logType, String logSubType, String module, String msgContent, Date logTime) {
/*  83 */     this.className = className;
/*  84 */     this.methodName = methodName;
/*  85 */     this.lineNumber = lineNumber;
/*  86 */     this.logType = logType;
/*  87 */     this.logSubType = logSubType;
/*  88 */     this.module = module;
/*  89 */     this.msgContent = msgContent;
/*  90 */     this.logTime = logTime;
/*     */   }
/*     */   
/*     */   public BizLogOpsModel(String className, String methodName, int lineNumber, String logType, String logSubType, String module, String msgContent, Date logTime, String traceId) {
/*  94 */     this.className = className;
/*  95 */     this.methodName = methodName;
/*  96 */     this.lineNumber = lineNumber;
/*  97 */     this.logType = logType;
/*  98 */     this.logSubType = logSubType;
/*  99 */     this.module = module;
/* 100 */     this.msgContent = msgContent;
/* 101 */     this.logTime = logTime;
/* 102 */     this.traceId = traceId;
/*     */   }
/*     */ 
/*     */   
/*     */   public BizLogOpsModel(String className, String methodName, int lineNumber, String logSubType, String module, String msgContent, Date logTime, String traceId) {
/* 107 */     this.className = className;
/* 108 */     this.methodName = methodName;
/* 109 */     this.lineNumber = lineNumber;
/* 110 */     this.logSubType = logSubType;
/* 111 */     this.module = module;
/* 112 */     this.msgContent = msgContent;
/* 113 */     this.logTime = logTime;
/* 114 */     this.traceId = traceId;
/*     */   }
/*     */   
/*     */   public String getClassName() {
/* 118 */     return this.className;
/*     */   }
/*     */   
/*     */   public void setClassName(String className) {
/* 122 */     this.className = className;
/*     */   }
/*     */   
/*     */   public String getMethodName() {
/* 126 */     return this.methodName;
/*     */   }
/*     */   
/*     */   public void setMethodName(String methodName) {
/* 130 */     this.methodName = methodName;
/*     */   }
/*     */   
/*     */   public String getLogType() {
/* 134 */     return this.logType;
/*     */   }
/*     */   
/*     */   public void setLogType(String logType) {
/* 138 */     this.logType = logType;
/*     */   }
/*     */   
/*     */   public String getLogSubType() {
/* 142 */     return this.logSubType;
/*     */   }
/*     */   
/*     */   public void setLogSubType(String logSubType) {
/* 146 */     this.logSubType = logSubType;
/*     */   }
/*     */   
/*     */   public String getModule() {
/* 150 */     return this.module;
/*     */   }
/*     */   
/*     */   public void setModule(String module) {
/* 154 */     this.module = module;
/*     */   }
/*     */   
/*     */   public String getMsgContent() {
/* 158 */     return this.msgContent;
/*     */   }
/*     */   
/*     */   public void setMsgContent(String msgContent) {
/* 162 */     this.msgContent = msgContent;
/*     */   }
/*     */   
/*     */   public Date getLogTime() {
/* 166 */     return this.logTime;
/*     */   }
/*     */   
/*     */   public void setLogTime(Date logTime) {
/* 170 */     this.logTime = logTime;
/*     */   }
/*     */   
/*     */   public int getLineNumber() {
/* 174 */     return this.lineNumber;
/*     */   }
/*     */   
/*     */   public void setLineNumber(int lineNumber) {
/* 178 */     this.lineNumber = lineNumber;
/*     */   }
/*     */   
/*     */   public String getTraceId() {
/* 182 */     return this.traceId;
/*     */   }
/*     */   
/*     */   public void setTraceId(String traceId) {
/* 186 */     this.traceId = traceId;
/*     */   }
/*     */ 
/*     */   
/*     */   public String toString() {
/* 191 */     return "BizLogOpsModel{className='" + this.className + '\'' + ", methodName='" + this.methodName + '\'' + ", lineNumber=" + this.lineNumber + ", logType='" + this.logType + '\'' + ", logSubType='" + this.logSubType + '\'' + ", module='" + this.module + '\'' + ", msgContent='" + this.msgContent + '\'' + ", traceId='" + this.traceId + '\'' + ", logTime=" + this.logTime + '}';
/*     */   }
/*     */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\event\BizLogOpsModel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */