package com.pl.event;

import java.util.Date;

public class MethodExceptionModel {
    private String className;
    private String methodName;
    private String requestParam;
    private String requestHeader;
    private int lineNumber;
    private String message;
    private String stackTraceMessage;
    private Date logTime = new Date();


    private String traceId;


    public MethodExceptionModel() {
    }


    public MethodExceptionModel(String className, String methodName, int lineNumber, String message, String stackTraceMessage, String requestHeader, String requestParam) {
        /*  48 */
        this.className = className;
        /*  49 */
        this.methodName = methodName;
        /*  50 */
        this.lineNumber = lineNumber;
        /*  51 */
        this.message = message;
        /*  52 */
        this.stackTraceMessage = stackTraceMessage;
        /*  53 */
        this.requestHeader = requestHeader;
        /*  54 */
        this.requestParam = requestParam;
    }


    public MethodExceptionModel(String className, String methodName, int lineNumber, String message, String stackTraceMessage, String requestHeader, String requestParam, String traceId) {
        /*  59 */
        this.className = className;
        /*  60 */
        this.methodName = methodName;
        /*  61 */
        this.lineNumber = lineNumber;
        /*  62 */
        this.message = message;
        /*  63 */
        this.stackTraceMessage = stackTraceMessage;
        /*  64 */
        this.requestHeader = requestHeader;
        /*  65 */
        this.requestParam = requestParam;
        /*  66 */
        this.traceId = traceId;
    }

    public String getClassName() {
        /*  69 */
        return this.className;
    }

    public void setClassName(String className) {
        /*  73 */
        this.className = className;
    }

    public String getMethodName() {
        /*  77 */
        return this.methodName;
    }

    public void setMethodName(String methodName) {
        /*  81 */
        this.methodName = methodName;
    }

    public int getLineNumber() {
        /*  85 */
        return this.lineNumber;
    }

    public void setLineNumber(int lineNumber) {
        /*  89 */
        this.lineNumber = lineNumber;
    }

    public String getMessage() {
        /*  93 */
        return this.message;
    }

    public void setMessage(String message) {
        /*  97 */
        this.message = message;
    }

    public String getStackTraceMessage() {
        /* 101 */
        return this.stackTraceMessage;
    }

    public void setStackTraceMessage(String stackTraceMessage) {
        /* 105 */
        this.stackTraceMessage = stackTraceMessage;
    }

    public Date getLogTime() {
        /* 109 */
        return this.logTime;
    }

    public void setLogTime(Date logTime) {
        /* 113 */
        this.logTime = logTime;
    }

    public String getRequestParam() {
        /* 117 */
        return this.requestParam;
    }

    public void setRequestParam(String requestParam) {
        /* 121 */
        this.requestParam = requestParam;
    }

    public String getRequestHeader() {
        /* 125 */
        return this.requestHeader;
    }

    public void setRequestHeader(String requestHeader) {
        /* 129 */
        this.requestHeader = requestHeader;
    }

    public String getTraceId() {
        /* 133 */
        return this.traceId;
    }

    public void setTraceId(String traceId) {
        /* 137 */
        this.traceId = traceId;
    }


    public String toString() {
        /* 142 */
        return "MethodExceptionModel{className='" + this.className + '\'' + ", methodName='" + this.methodName + '\'' + ", requestParam='" + this.requestParam + '\'' + ", requestHeader='" + this.requestHeader + '\'' + ", lineNumber=" + this.lineNumber + ", message='" + this.message + '\'' + ", stackTraceMessage='" + this.stackTraceMessage + '\'' + ", logTime=" + this.logTime + ", traceId='" + this.traceId + '\'' + '}';
    }
}


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\event\MethodExceptionModel.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */