/*    */ package com.pl.event;
/*    */ 
/*    */ import com.fasterxml.jackson.annotation.JsonTypeInfo;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, property = "type")
/*    */ public abstract class AbstractOpsEvent
/*    */ {
/*    */   private String project;
/*    */   private String application;
/*    */   
/*    */   protected AbstractOpsEvent() {}
/*    */   
/*    */   public AbstractOpsEvent(String project, String application) {
/* 25 */     this.project = project;
/* 26 */     this.application = application;
/*    */   }
/*    */   
/*    */   public String getProject() {
/* 30 */     return this.project;
/*    */   }
/*    */   
/*    */   public void setProject(String project) {
/* 34 */     this.project = project;
/*    */   }
/*    */   
/*    */   public String getApplication() {
/* 38 */     return this.application;
/*    */   }
/*    */   
/*    */   public void setApplication(String application) {
/* 42 */     this.application = application;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\event\AbstractOpsEvent.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */