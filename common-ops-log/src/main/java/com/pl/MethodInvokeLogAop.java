package com.pl;

import cn.hutool.core.exceptions.ExceptionUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.lang.annotation.Annotation;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import com.pl.annotation.MethodInvokeLogOps;
import com.pl.client.IOpsClient;
import com.pl.configuration.OpsProperties;
import com.pl.event.MethodInvokeLogOpsEvent;
import com.pl.log.InvokeLogUtil;
import com.pl.log.MethodInvokeLog;
import com.pl.util.HttpRequestUtil;
import com.pl.util.ObjectMapperUtil;
import com.pl.valid.ValidUtil;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;


@Order(1)
@Aspect
@Component
public class MethodInvokeLogAop {
    /*  45 */   private final Logger log = LoggerFactory.getLogger(OpsAop.class);

    /*  47 */   private ObjectMapper mapper = ObjectMapperUtil.createObjectMapper();


    @Autowired
    private IOpsClient agentClient;

    @Autowired
    private OpsJob opsJob;

    @Autowired
    private OpsProperties opsProperties;

    @Autowired
    private ValidUtil validUtil;


    @Pointcut("@annotation(methodInvokeLogOps)")
    public void methodInvokeLog(MethodInvokeLogOps methodInvokeLogOps) {
    }


    @Around("methodInvokeLog(methodInvokeLogOps)")
    public Object invokeAround(ProceedingJoinPoint point, MethodInvokeLogOps methodInvokeLogOps) throws Throwable {
        /*  69 */
        long start = System.currentTimeMillis();
        /*  70 */
        Date invokeTime = new Date();
        /*  71 */
        StringBuilder methodBuilder = new StringBuilder();
        /*  72 */
        String className = point.getTarget().getClass().getName();
        /*  73 */
        String methodName = point.getSignature().getName();
        /*  74 */
        methodBuilder.append(className)
/*  75 */.append(".")
/*  76 */.append(methodName);

        /*  78 */
        String method = methodBuilder.toString();

        /*  80 */
        methodInvokeLogOps = (MethodInvokeLogOps) AnnotationUtils.getAnnotation((Annotation) methodInvokeLogOps, MethodInvokeLogOps.class);
        /*  81 */
        String methodDesc = methodInvokeLogOps.desc();

        /*  83 */
        boolean logParams = methodInvokeLogOps.isLogParams();

        /*  85 */
        String methodType = methodInvokeLogOps.type();
        /*  86 */
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        /*  88 */
        HttpServletRequest request = null;
        /*  89 */
        if (requestAttributes != null) {
            /*  90 */
            Object requestObj = requestAttributes.resolveReference("request");
            /*  91 */
            if (requestObj instanceof HttpServletRequest) {
                /*  92 */
                request = (HttpServletRequest) requestObj;
            }
        }


        /*  97 */
        MethodInvokeLog invokeLog = getCurrentInvokeLog(request, method);

        /*  99 */
        InvokeLogUtil.setMethodInvokeLog(invokeLog);

        /* 101 */
        String uri = (request == null) ? null : request.getRequestURI();

        /* 103 */
        MethodSignature methodSignature = (MethodSignature) point.getSignature();
        /* 104 */
        String[] parameterNames = methodSignature.getParameterNames();


        /* 107 */
        String responseBody = null;
        /* 108 */
        String errorMsg = null;
        /* 109 */
        int invokeResult = 0;

        try {
            /* 112 */
            Object result = point.proceed();

            try {
                /* 115 */
                responseBody = (result != null) ? this.mapper.writeValueAsString(result) : null;
                /* 116 */
            } catch (JsonProcessingException e) {
                /* 117 */
                this.log.warn("serialize result error", (Throwable) e);
            }

            /* 120 */
            return result;
            /* 121 */
        } catch (Throwable e) {
            /* 122 */
            invokeResult = 1;
            /* 123 */
            errorMsg = e.getMessage() + "\n" + ExceptionUtil.stacktraceToString(e);
            /* 124 */
            throw e;
        } finally {
            try {
                /* 127 */
                long duration = System.currentTimeMillis() - start;





                /* 133 */
                MethodInvokeLogOpsEvent event = new MethodInvokeLogOpsEvent(this.opsJob.getProject(), this.opsJob.getApplication(), invokeLog.getTraceId(), className, methodName, invokeLog.getLevel(), invokeLog.getParentId(), (logParams || this.opsProperties.getDebug().booleanValue()) ? HttpRequestUtil.getRequestParam(point) : null, (logParams || this.opsProperties.getDebug().booleanValue()) ? responseBody : null, errorMsg, invokeResult, invokeTime, duration, uri, methodDesc, methodType, HttpRequestUtil.getRequestHeader());

                /* 135 */
                MethodInvokeLogOpsEvent logOpsEvent = this.validUtil.vaidRule(event);
                /* 136 */
                this.agentClient.sendMethodInvokeLogOpsEvent(logOpsEvent);
            } finally {

                /* 140 */
                InvokeLogUtil.removeMethodInvokeLog();
            }
        }
    }


    private MethodInvokeLog getCurrentInvokeLog(HttpServletRequest request, String method) {
        /* 148 */
        MethodInvokeLog invokeLog, currentInvokeLog = InvokeLogUtil.getMethodInvokeLog();
        /* 149 */
        if (currentInvokeLog != null) {
            /* 151 */
            return new MethodInvokeLog(currentInvokeLog.getTraceId(), method, currentInvokeLog.getLevel() + 1, currentInvokeLog.getMethodId());
        }


        /* 155 */
        if (request == null || StrUtil.isBlank(request.getHeader("trace-id"))) {

            /* 157 */
            invokeLog = new MethodInvokeLog(method);
        } else {

            /* 160 */
            String traceId = request.getHeader("trace-id");
            /* 161 */
            String invokerId = request.getHeader("invoker-id");
            /* 162 */
            int level = Integer.valueOf(request.getHeader("trace-level")).intValue();

            /* 164 */
            invokeLog = new MethodInvokeLog(traceId, method, level + 1, invokerId);
        }

        /* 167 */
        return invokeLog;
    }
}


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\MethodInvokeLogAop.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */