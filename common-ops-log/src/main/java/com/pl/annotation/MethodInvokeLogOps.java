package com.pl.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.springframework.core.annotation.AliasFor;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface MethodInvokeLogOps {
  @AliasFor("desc")
  String value() default "";
  
  @AliasFor("value")
  String desc() default "";
  
  String type() default "";
  
  boolean isLogParams() default true;
}


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\annotation\MethodInvokeLogOps.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */