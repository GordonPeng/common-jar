/*    */ package com.pl.interceptor;
/*    */ 
/*    */ import com.pl.log.InvokeLogUtil;
import com.pl.log.MethodInvokeLog;
import feign.RequestInterceptor;
/*    */ import feign.RequestTemplate;
/*    */ import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
/*    */ import org.springframework.stereotype.Component;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @ConditionalOnClass({RequestInterceptor.class})
/*    */ @Component
/*    */ public class FeignInvokerInterceptor
/*    */   implements RequestInterceptor
/*    */ {
/*    */   public void apply(RequestTemplate template) {
/* 27 */     MethodInvokeLog invokeLog = InvokeLogUtil.getMethodInvokeLog();
/* 28 */     if (invokeLog != null) {
/* 29 */       template.header("trace-id", new String[] { invokeLog.getTraceId() });
/* 30 */       template.header("trace-level", new String[] { String.valueOf(invokeLog.getLevel()) });
/* 31 */       template.header("invoker-id", new String[] { invokeLog.getMethodId() });
/*    */     } 
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\interceptor\FeignInvokerInterceptor.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */