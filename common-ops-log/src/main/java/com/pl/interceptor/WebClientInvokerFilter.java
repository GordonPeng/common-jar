/*    */ package com.pl.interceptor;
/*    */ 
/*    */ import com.pl.log.InvokeLogUtil;
import com.pl.log.MethodInvokeLog;
import org.springframework.web.context.request.RequestAttributes;
/*    */ import org.springframework.web.context.request.RequestContextHolder;
/*    */ import org.springframework.web.reactive.function.client.ClientRequest;
/*    */ import org.springframework.web.reactive.function.client.ClientResponse;
/*    */ import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
/*    */ import org.springframework.web.reactive.function.client.ExchangeFunction;
/*    */ import reactor.core.publisher.Mono;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class WebClientInvokerFilter
/*    */   implements ExchangeFilterFunction
/*    */ {
/*    */   public Mono<ClientResponse> filter(ClientRequest req, ExchangeFunction next) {
/* 25 */     MethodInvokeLog invokeLog = InvokeLogUtil.getMethodInvokeLog();
/* 26 */     if (invokeLog != null) {
/* 27 */       ClientRequest.Builder request = ClientRequest.from(req);
/* 28 */       RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
/* 29 */       if (requestAttributes != null && requestAttributes instanceof org.springframework.web.context.request.ServletRequestAttributes) {
/* 30 */         request.header("trace-id", new String[] { invokeLog.getTraceId() });
/*    */         
/* 32 */         request.header("invoker-id", new String[] { invokeLog.getMethodId() });
/*    */         
/* 34 */         request.header("trace-level", new String[] { String.valueOf(invokeLog.getLevel()) });
/*    */       } 
/* 36 */       return next.exchange(request.build());
/*    */     } 
/*    */     
/* 39 */     return next.exchange(req);
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\interceptor\WebClientInvokerFilter.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */