/*    */ package com.pl.interceptor;
/*    */ 
/*    */ import java.io.IOException;
/*    */ import com.pl.log.InvokeLogUtil;
import com.pl.log.MethodInvokeLog;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
/*    */ import org.springframework.http.HttpHeaders;
/*    */ import org.springframework.http.HttpRequest;
/*    */ import org.springframework.http.client.ClientHttpRequestExecution;
/*    */ import org.springframework.http.client.ClientHttpRequestInterceptor;
/*    */ import org.springframework.http.client.ClientHttpResponse;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @ConditionalOnClass({ClientHttpRequestInterceptor.class})
/*    */ public class RestTemplateInvokerInterceptor
/*    */   implements ClientHttpRequestInterceptor
/*    */ {
/*    */   public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
/* 27 */     MethodInvokeLog invokeLog = InvokeLogUtil.getMethodInvokeLog();
/* 28 */     if (invokeLog != null) {
/* 29 */       HttpHeaders headers = request.getHeaders();
/* 30 */       headers.add("trace-id", invokeLog.getTraceId());
/*    */       
/* 32 */       headers.add("invoker-id", invokeLog.getMethodId());
/*    */       
/* 34 */       headers.add("trace-level", String.valueOf(invokeLog.getLevel()));
/*    */     } 
/*    */     
/* 37 */     return execution.execute(request, body);
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\interceptor\RestTemplateInvokerInterceptor.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */