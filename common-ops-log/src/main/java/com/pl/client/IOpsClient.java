package com.pl.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.pl.event.BizLogOpsEvent;
import com.pl.event.ExtApiLogOpsEvent;
import com.pl.event.MethodInvokeLogOpsEvent;
import com.pl.event.MethodOpsEvent;

public interface IOpsClient {
  void sendMethodOpsEvent(MethodOpsEvent paramMethodOpsEvent) throws JsonProcessingException;
  
  void sendBizLogOpsEvent(BizLogOpsEvent paramBizLogOpsEvent) throws JsonProcessingException;
  
  void sendMethodInvokeLogOpsEvent(MethodInvokeLogOpsEvent paramMethodInvokeLogOpsEvent) throws JsonProcessingException;
  
  void sendExtApiLogOpsEvent(ExtApiLogOpsEvent paramExtApiLogOpsEvent) throws JsonProcessingException;
}

