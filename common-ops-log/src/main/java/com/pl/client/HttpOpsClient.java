/*     */ package com.pl.client;
/*     */ 
/*     */ import java.nio.charset.Charset;
/*     */ import java.nio.charset.StandardCharsets;
/*     */ import java.time.Duration;
/*     */ import javax.annotation.PostConstruct;
/*     */ import com.pl.OpsJob;
import com.pl.configuration.OpsProperties;
import com.pl.event.BizLogOpsEvent;
import com.pl.event.ExtApiLogOpsEvent;
import com.pl.event.MethodInvokeLogOpsEvent;
import com.pl.event.MethodOpsEvent;
import org.slf4j.Logger;
/*     */ import org.slf4j.LoggerFactory;
/*     */ import org.springframework.beans.factory.annotation.Autowired;
/*     */ import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
/*     */ import org.springframework.http.MediaType;
/*     */ import org.springframework.stereotype.Service;
/*     */ import org.springframework.web.reactive.function.client.WebClient;
/*     */ import reactor.core.publisher.Mono;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ @ConditionalOnProperty(name = {"mos.ops.client-type"}, havingValue = "http")
/*     */ @Service
/*     */ public class HttpOpsClient
/*     */   implements IOpsClient
/*     */ {
/*  34 */   private final Logger log = LoggerFactory.getLogger(OpsJob.class);
/*     */   
/*     */   @Autowired
/*     */   private WebClient webClient;
/*     */   
/*     */   @Autowired
/*     */   private OpsProperties opsProperties;
/*     */   
/*     */   private String baseUrl;
/*     */   
/*     */   public static final String SEND_METHOD_OPS_EVENT_URI = "%s/msg/method-ops";
/*     */   
/*     */   public static final String SEND_BIZLOG_OPS_EVENT_URI = "%s/msg/bizlog-ops";
/*     */   
/*     */   public static final String SEND_METHOD_INVOKE_LOG_OPS_EVENT_URI = "%s/msg/method-invoke-log-ops";
/*     */   
/*     */   public static final String SEND_EXT_API_LOG_OPS_EVENT_URI = "%s/msg/ext-api-log-ops";
/*     */ 
/*     */   
/*     */   @PostConstruct
/*     */   public void init() {
/*  55 */     this.baseUrl = this.opsProperties.getAgentBaseUri();
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public <K, T> T postInvoke(String uri, K k, Class<T> responseClass) {
/*     */     try {
/*  69 */       Mono<T> mono = ((WebClient.RequestBodySpec)this.webClient.post().uri(uri, new Object[0])).syncBody(k).header("Content-Type", new String[] { "application/json" }).accept(new MediaType[] { MediaType.APPLICATION_JSON }).acceptCharset(new Charset[] { StandardCharsets.UTF_8 }).retrieve().bodyToMono(responseClass);
/*     */       
/*  71 */       return (T)mono.block(Duration.ofSeconds(10L));
/*  72 */     } catch (Exception e) {
/*  73 */       this.log.warn("postInvoke uri: {} error, request = {}", uri, k);
/*  74 */       this.log.warn("error", e);
/*     */ 
/*     */       
/*  77 */       return null;
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   public void sendMethodOpsEvent(MethodOpsEvent event) {
/*  83 */     String uri = String.format("%s/msg/method-ops", new Object[] { this.baseUrl });
/*     */     
/*  85 */     postInvoke(uri, event, Void.class);
/*     */   }
/*     */ 
/*     */   
/*     */   public void sendBizLogOpsEvent(BizLogOpsEvent event) {
/*  90 */     String uri = String.format("%s/msg/bizlog-ops", new Object[] { this.baseUrl });
/*     */     
/*  92 */     postInvoke(uri, event, Void.class);
/*     */   }
/*     */ 
/*     */   
/*     */   public void sendMethodInvokeLogOpsEvent(MethodInvokeLogOpsEvent event) {
/*  97 */     String uri = String.format("%s/msg/method-invoke-log-ops", new Object[] { this.baseUrl });
/*     */     
/*  99 */     postInvoke(uri, event, Void.class);
/*     */   }
/*     */ 
/*     */   
/*     */   public void sendExtApiLogOpsEvent(ExtApiLogOpsEvent event) {
/* 104 */     String uri = String.format("%s/msg/ext-api-log-ops", new Object[] { this.baseUrl });
/*     */     
/* 106 */     postInvoke(uri, event, Void.class);
/*     */   }
/*     */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\client\HttpOpsClient.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */