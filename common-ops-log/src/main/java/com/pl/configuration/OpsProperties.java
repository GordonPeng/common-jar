/*    */ package com.pl.configuration;
/*    */ 
/*    */ import org.springframework.boot.context.properties.ConfigurationProperties;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ @ConfigurationProperties(prefix = "mos.ops")
/*    */ public class OpsProperties
/*    */ {
/*    */   private String agentBaseUri;
/* 18 */   private EAgentClientType clientType = EAgentClientType.MQ;
/*    */ 
/*    */ 
/*    */   
/*    */   private String mqTopic;
/*    */ 
/*    */ 
/*    */   
/* 26 */   private Boolean debug = Boolean.valueOf(false);
/*    */ 
/*    */   
/*    */   public String getAgentBaseUri() {
/* 30 */     return this.agentBaseUri;
/*    */   }
/*    */   
/*    */   public void setAgentBaseUri(String agentBaseUri) {
/* 34 */     this.agentBaseUri = agentBaseUri;
/*    */   }
/*    */   
/*    */   public EAgentClientType getClientType() {
/* 38 */     return this.clientType;
/*    */   }
/*    */   
/*    */   public void setClientType(EAgentClientType clientType) {
/* 42 */     this.clientType = clientType;
/*    */   }
/*    */   
/*    */   public String getMqTopic() {
/* 46 */     return this.mqTopic;
/*    */   }
/*    */   
/*    */   public void setMqTopic(String mqTopic) {
/* 50 */     this.mqTopic = mqTopic;
/*    */   }
/*    */   
/*    */   public Boolean getDebug() {
/* 54 */     return this.debug;
/*    */   }
/*    */   
/*    */   public void setDebug(Boolean debug) {
/* 58 */     this.debug = debug;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\configuration\OpsProperties.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */