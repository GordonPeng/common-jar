/*    */ package com.pl.valid;
/*    */ 
/*    */ import java.util.List;
/*    */ 
/*    */ public class Rules {
/*    */   private String methodName;
/*    */   private List<RuleField> input;
/*    */   private List<RuleField> output;
/*    */   
/*    */   public String getMethodName() {
/* 11 */     return this.methodName;
/*    */   }
/*    */   
/*    */   public void setMethodName(String methodName) {
/* 15 */     this.methodName = methodName;
/*    */   }
/*    */   
/*    */   public List<RuleField> getInput() {
/* 19 */     return this.input;
/*    */   }
/*    */   
/*    */   public void setInput(List<RuleField> input) {
/* 23 */     this.input = input;
/*    */   }
/*    */   
/*    */   public List<RuleField> getOutput() {
/* 27 */     return this.output;
/*    */   }
/*    */   
/*    */   public void setOutput(List<RuleField> output) {
/* 31 */     this.output = output;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\valid\Rules.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */