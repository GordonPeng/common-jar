/*    */ package com.pl.valid;
/*    */ 
/*    */ public class RuleField {
/*    */   private String fieldName;
/*    */   private boolean display;
/*    */   
/*    */   public String getFieldName() {
/*  8 */     return this.fieldName;
/*    */   }
/*    */   
/*    */   public void setFieldName(String fieldName) {
/* 12 */     this.fieldName = fieldName;
/*    */   }
/*    */   
/*    */   public boolean isDisplay() {
/* 16 */     return this.display;
/*    */   }
/*    */   
/*    */   public void setDisplay(boolean display) {
/* 20 */     this.display = display;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\valid\RuleField.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */