/*    */ package com.pl.valid;
/*    */ 
/*    */ import java.util.List;
/*    */ import org.springframework.boot.context.properties.ConfigurationProperties;
/*    */ import org.springframework.context.annotation.PropertySource;
/*    */ import org.springframework.stereotype.Component;
/*    */ 
/*    */ @Component
/*    */ @PropertySource(value = {"classpath:valid/valid.yml"}, factory = YamlPropertyLoaderFactory.class, encoding = "UTF-8")
/*    */ @ConfigurationProperties(prefix = "valid")
/*    */ public class OpsLogRule
/*    */ {
/*    */   private String project;
/*    */   private List<Rules> rules;
/*    */   
/*    */   public String getProject() {
/* 17 */     return this.project;
/*    */   }
/*    */   
/*    */   public void setProject(String project) {
/* 21 */     this.project = project;
/*    */   }
/*    */ 
/*    */   
/*    */   public List<Rules> getRules() {
/* 26 */     return this.rules;
/*    */   }
/*    */   
/*    */   public void setRules(List<Rules> rules) {
/* 30 */     this.rules = rules;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\valid\OpsLogRule.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */