/*    */ package com.pl.valid;
/*    */ 
/*    */ import java.io.IOException;
/*    */ import org.springframework.boot.env.YamlPropertySourceLoader;
/*    */ import org.springframework.core.env.PropertySource;
/*    */ import org.springframework.core.io.support.DefaultPropertySourceFactory;
/*    */ import org.springframework.core.io.support.EncodedResource;
/*    */ 
/*    */ public class YamlPropertyLoaderFactory
/*    */   extends DefaultPropertySourceFactory
/*    */ {
/*    */   public PropertySource<?> createPropertySource(String name, EncodedResource resource) throws IOException {
/* 13 */     if (resource == null) {
/* 14 */       return super.createPropertySource(name, resource);
/*    */     }
/*    */     
/* 17 */     return (new YamlPropertySourceLoader()).load(resource.getResource().getFilename(), resource.getResource()).get(0);
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\valid\YamlPropertyLoaderFactory.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */