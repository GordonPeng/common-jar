package com.pl.valid;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;

import java.util.List;

import com.pl.event.MethodInvokeLogOpsEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class ValidUtil {
    /* 16 */   private final Logger log = LoggerFactory.getLogger(ValidUtil.class);
    @Autowired
    private OpsLogRule opsLogRule;

    public MethodInvokeLogOpsEvent vaidRule(MethodInvokeLogOpsEvent event) {
        /* 21 */
        MethodInvokeLogOpsEvent methodInvokeLogOpsEvent = new MethodInvokeLogOpsEvent();
        /* 22 */
        BeanUtil.copyProperties(event, methodInvokeLogOpsEvent, new String[0]);
        /* 23 */
        List<Rules> rulesList = this.opsLogRule.getRules();
        /* 24 */
        for (Rules rules : rulesList) {

            /* 26 */
            if (!rules.getMethodName().equals(event.getMethodName())) {
                /* 27 */
                return methodInvokeLogOpsEvent;
            }
            /* 29 */
            List<RuleField> inputFields = rules.getInput();

            /* 31 */
            handleRule(event, inputFields, 0);
            /* 32 */
            this.log.info("event:{}", JSONUtil.toJsonStr(event));
            /* 33 */
            if (!StringUtils.hasLength(event.getRequestParams())) {
                /* 34 */
                return methodInvokeLogOpsEvent;
            }
            /* 36 */
            methodInvokeLogOpsEvent.setRequestParams(event.getRequestParams());
            /* 37 */
            List<RuleField> outputFields = rules.getOutput();

            /* 39 */
            handleRule(event, outputFields, 1);
            /* 40 */
            if (!StringUtils.hasLength(event.getResponseBody())) {
                /* 41 */
                return methodInvokeLogOpsEvent;
            }
            /* 43 */
            methodInvokeLogOpsEvent.setResponseBody(event.getResponseBody());
        }
        /* 45 */
        return methodInvokeLogOpsEvent;
    }

    private void handleRule(MethodInvokeLogOpsEvent event, List<RuleField> inputFields, int flag) {
        /* 49 */
        for (RuleField ruleField : inputFields) {

            /* 51 */
            if (!StringUtils.hasLength(event.getRequestParams())) {
                return;
            }
            /* 54 */
            if (!StringUtils.hasLength(event.getResponseBody())) {
                return;
            }
            /* 57 */
            String params = (flag == 0) ? event.getRequestParams() : event.getResponseBody();
            /* 58 */
            if (!params.contains(ruleField.getFieldName())) {
                continue;
            }
            /* 61 */
            if (!ruleField.isDisplay()) {
                /* 62 */
                int start = params.indexOf(ruleField.getFieldName()) - 1;
                /* 63 */
                int end = params.indexOf(",", start) + 1;
                /* 64 */
                StringBuffer rep = new StringBuffer(params);
                /* 65 */
                String result = rep.replace(start, end, "").toString();
                /* 66 */
                if (flag == 0) {
                    /* 67 */
                    event.setRequestParams(result);
                    continue;
                }
                /* 69 */
                event.setResponseBody(result);
            }
        }
    }
}


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\valid\ValidUtil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */