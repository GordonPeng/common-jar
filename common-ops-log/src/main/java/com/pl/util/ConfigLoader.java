/*    */ package com.pl.util;
/*    */ 
/*    */ import java.io.BufferedReader;
/*    */ import java.io.InputStreamReader;
/*    */ import java.net.URL;
/*    */ import java.nio.charset.Charset;
/*    */ import java.util.ArrayList;
/*    */ import java.util.Enumeration;
/*    */ import java.util.List;
/*    */ import java.util.Map;
/*    */ import java.util.Properties;
/*    */ import java.util.concurrent.CopyOnWriteArraySet;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ConfigLoader
/*    */ {
/*    */   private static final String DEFAULT_OPS_CONFIG_FILE = "classpath:ops.properties";
/*    */   public static final String CLASSPATH_FILE_FLAG = "classpath:";
/* 39 */   private static Properties properties = new Properties();
/*    */   
/*    */   static {
/* 42 */     loadConfig();
/*    */   }
/*    */   
/*    */   private static void loadConfig() {
/* 46 */     Properties p = loadPropertiesFromClasspathFile("classpath:ops.properties");
/* 47 */     if (p != null && !p.isEmpty()) {
/* 48 */       properties.putAll(p);
/*    */     }
/* 50 */     for (Map.Entry<Object, Object> entry : (Iterable<Map.Entry<Object, Object>>)new CopyOnWriteArraySet(System.getProperties().entrySet())) {
/* 51 */       String configKey = entry.getKey().toString();
/* 52 */       String newConfigValue = entry.getValue().toString();
/* 53 */       properties.put(configKey, newConfigValue);
/*    */     } 
/*    */   }
/*    */   
/*    */   private static Properties loadPropertiesFromClasspathFile(String fileName) {
/* 58 */     fileName = fileName.substring("classpath:".length()).trim();
/*    */     
/* 60 */     List<URL> list = new ArrayList<>();
/*    */     try {
/* 62 */       Enumeration<URL> urls = getClassLoader().getResources(fileName);
/* 63 */       list = new ArrayList<>();
/* 64 */       while (urls.hasMoreElements()) {
/* 65 */         list.add(urls.nextElement());
/*    */       }
/* 67 */     } catch (Throwable e) {
/* 68 */       e.printStackTrace();
/*    */     } 
/*    */     
/* 71 */     if (list.isEmpty()) {
/* 72 */       return null;
/*    */     }
/*    */     
/* 75 */     Properties properties = new Properties();
/* 76 */     for (URL url : list) {
/* 77 */       try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url
/* 78 */               .openStream(), Charset.forName("UTF-8")))) {
/* 79 */         Properties p = new Properties();
/* 80 */         p.load(bufferedReader);
/* 81 */         properties.putAll(p);
/* 82 */       } catch (Throwable e) {
/* 83 */         e.printStackTrace();
/*    */       } 
/*    */     } 
/* 86 */     return properties;
/*    */   }
/*    */   
/*    */   private static ClassLoader getClassLoader() {
/* 90 */     ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
/* 91 */     if (classLoader == null) {
/* 92 */       classLoader = ConfigLoader.class.getClassLoader();
/*    */     }
/* 94 */     return classLoader;
/*    */   }
/*    */   
/*    */   public static Properties getProperties() {
/* 98 */     return properties;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\lo\\util\ConfigLoader.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */