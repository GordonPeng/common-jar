/*    */ package com.pl.util;
/*    */ 
/*    */ import cn.hutool.core.date.DateUtil;
/*    */ import java.util.Date;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class TimeUtil
/*    */ {
/*    */   public static long currentTimeMillis() {
/* 35 */     return System.currentTimeMillis();
/*    */   }
/*    */   
/*    */   public static String currentTimeMinuteStr() {
/* 39 */     return DateUtil.format(new Date(), "yyyy-MM-dd HH:mm");
/*    */   }
/*    */   
/*    */   public static String lastMinuteTimeMuniteStr() {
/* 43 */     return DateUtil.format((Date)DateUtil.offsetMinute(new Date(), -1), "yyyy-MM-dd HH:mm");
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\lo\\util\TimeUtil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */