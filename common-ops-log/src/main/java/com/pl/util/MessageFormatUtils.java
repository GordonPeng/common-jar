/*    */ package com.pl.util;
/*    */ 
/*    */ import org.slf4j.helpers.FormattingTuple;
/*    */ import org.slf4j.helpers.MessageFormatter;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class MessageFormatUtils
/*    */ {
/*    */   public static String format(String format, Object arg) {
/* 42 */     FormattingTuple ft = MessageFormatter.format(format, arg);
/* 43 */     return ft.getMessage();
/*    */   }
/*    */   
/*    */   public static String format(String format, Object arg1, Object arg2) {
/* 47 */     FormattingTuple ft = MessageFormatter.format(format, arg1, arg2);
/* 48 */     return ft.getMessage();
/*    */   }
/*    */   
/*    */   public static String format(String format, Object... argArray) {
/* 52 */     FormattingTuple ft = MessageFormatter.arrayFormat(format, argArray);
/* 53 */     return ft.getMessage();
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\lo\\util\MessageFormatUtils.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */