/*    */ package com.pl.util;
/*    */ 
/*    */ import com.fasterxml.jackson.annotation.JsonInclude;
/*    */ import com.fasterxml.jackson.databind.DeserializationFeature;
/*    */ import com.fasterxml.jackson.databind.ObjectMapper;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ObjectMapperUtil
/*    */ {
/*    */   public static ObjectMapper createObjectMapper() {
/* 16 */     ObjectMapper mapper = new ObjectMapper();
/* 17 */     mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
/* 18 */     mapper.setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL);
/*    */     
/* 20 */     return mapper;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\lo\\util\ObjectMapperUtil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */