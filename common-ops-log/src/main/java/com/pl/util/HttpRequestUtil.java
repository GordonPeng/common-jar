package com.pl.util;

import cn.hutool.core.collection.CollectionUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;


public class HttpRequestUtil {
    private static List<String> DEFAULT_HEADERS = new ArrayList<>();

    private static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setDefaultPropertyInclusion(JsonInclude.Include.NON_EMPTY);

        ArrayList<String> headers = CollectionUtil.newArrayList(new String[]{"Accept", "Accept-Charset", "Accept-Encoding", "Accept-Language", "Accept-Ranges", "Access-Control-Allow-Credentials", "Access-Control-Allow-Headers", "Access-Control-Allow-Methods", "Access-Control-Allow-Origin", "Access-Control-Expose-Headers", "Access-Control-Max-Age", "Access-Control-Request-Headers", "Access-Control-Request-Method", "Age", "Allow", "Authorization", "Cache-Control", "Connection", "Content-Encoding", "Content-Disposition", "Content-Language", "Content-Length", "Content-Location", "Content-Range", "Content-Type", "Cookie", "Date", "ETag", "Expect", "Expires", "From", "Host", "If-Match", "If-Modified-Since", "If-None-Match", "If-Range", "If-Unmodified-Since", "Last-Modified", "Link", "Location", "Max-Forwards", "Origin", "Pragma", "Proxy-Authenticate", "Proxy-Authorization", "Range", "Referer", "Retry-After", "Server", "Set-Cookie", "Set-Cookie2", "TE", "Trailer", "Transfer-Encoding", "Upgrade", "User-Agent", "Vary", "Via", "Warning", "WWW-Authenticate", "WWW-Authenticate"});


        for (String header : headers) {
            DEFAULT_HEADERS.add(header.toLowerCase());
        }
    }


    public static String getRequestParam(ProceedingJoinPoint point) {
        MethodSignature methodSignature = (MethodSignature) point.getSignature();
        String[] parameterNames = methodSignature.getParameterNames();
        Object[] method_param = point.getArgs();
        StringBuilder requestParam = new StringBuilder();

        if (parameterNames != null) {
            for (int i = 0; i < method_param.length; i++) {
                if (i != 0) {
                    requestParam.append(", ");
                }
                requestParam.append("" + parameterNames[i] + " = ");
                if (method_param[i] != null && !(method_param[i] instanceof HttpServletRequest) && !(method_param[i] instanceof javax.servlet.http.HttpServletResponse) && !(method_param[i] instanceof org.springframework.web.multipart.MultipartFile)) {


                    try {


                        requestParam.append(mapper.writeValueAsString(method_param[i]));
                    } catch (JsonProcessingException e) {
                    }

                } else {


                    requestParam.append("null");
                }
            }
        }

        return requestParam.toString();
    }

    public static String getRequestHeader() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();

        HttpServletRequest request = null;
        if (requestAttributes != null) {
            Object requestObj = requestAttributes.resolveReference("request");
            if (requestObj instanceof HttpServletRequest) {
                request = (HttpServletRequest) requestObj;

                StringBuilder sb = new StringBuilder();
                Enumeration<String> enumeration = request.getHeaderNames();
                while (enumeration.hasMoreElements()) {
                    String headerName = enumeration.nextElement();
                    if (!DEFAULT_HEADERS.contains(headerName.toLowerCase())) {
                        String header = request.getHeader(headerName);
                        sb.append(headerName).append(": ").append(header).append("\n");
                    }
                }

                return sb.toString();
            }
        }

        return null;
    }
}


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\lo\\util\HttpRequestUtil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */