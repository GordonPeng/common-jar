package com.pl.util;

import cn.hutool.core.collection.ConcurrentHashSet;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;
import com.pl.event.BizLogOpsModel;
import com.pl.event.MethodExceptionModel;
import com.pl.limit.FlowLimiter;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


public class Constant {
    /* 42 */   public static final Map<String, FlowLimiter> GLOBAL_FLOW_LIMITER_MAP = new ConcurrentHashMap<>();


    /* 45 */   public static Set<String> methodSignatureSet = (Set<String>) new ConcurrentHashSet();


    /* 48 */   public static Multimap<String, MethodExceptionModel> exceptionMap = (Multimap<String, MethodExceptionModel>) Multimaps.synchronizedListMultimap((ListMultimap) LinkedListMultimap.create());


    /* 51 */   public static Multimap<String, BizLogOpsModel> GLOBAL_BIZ_LOG_MAP = (Multimap<String, BizLogOpsModel>) Multimaps.synchronizedListMultimap((ListMultimap) LinkedListMultimap.create());
    public static final String METHOD_INVOKE_PERFORMANCE_NAME = "method.invoke.perf";
    public static final String METHOD_INVOKE_COUNT_FAIL_NAME = "method.invoke.count.fail";
    public static final String METHOD_TAG_SIGNATURE = "method.tag.signature";
    public static final String METHOD_TAG_TIMESTAMP = "method.tag.timestamp";
    public static final String LOG_EXCEPTION_LIMITER_ID = "log.exception.limiter";
    public static final String LOG_BIZ_LIMITER_ID = "log.biz.limiter";
    public static final String HEADER_TRACE_ID = "trace-id";
    public static final String HEADER_INVOKER_ID = "invoker-id";
    public static final String HEADER_TRACE_LEVEL = "trace-level";
    public static final int INVOKE_RESULT_SUCCESS = 0;
    public static final int INVOKE_RESULT_FAIL = 1;
}
