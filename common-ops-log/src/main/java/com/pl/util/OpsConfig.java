/*     */ package com.pl.util;
/*     */ 
/*     */ import java.util.Properties;
/*     */ import org.slf4j.Logger;
/*     */ import org.slf4j.LoggerFactory;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class OpsConfig
/*     */ {
/*  33 */   private static final Logger log = LoggerFactory.getLogger(OpsConfig.class);
/*     */   
/*     */   public static final String CONFIG_KEY_PROJECT = "mos.ops.project";
/*     */   
/*     */   public static final String CONFIG_KEY_APPLICATION = "mos.ops.application";
/*     */   
/*     */   public static final String CONFIG_KEY_BIZLOGLIMIT = "mos.ops.limitPerSec.bizLog";
/*     */   
/*     */   public static final String CONFIG_KEY_ERRORLOGLIMIT = "mos.ops.limitPerSec.errorLog";
/*     */   
/*     */   public static final String CONFIG_KEY_ENVIRONMENT = "mos.ops.environment";
/*     */   
/*     */   private static final String PROJECT_DEFAULT = "default";
/*     */   
/*     */   private static final String APPLICATION_DEFAULT = "default";
/*     */   
/*     */   private static final long BIZLOG_LIMITPERSEC_DEFAULT = 1000L;
/*     */   
/*     */   private static final long ERRORLOG_LIMITPERSEC_DEFAULT = 1000L;
/*     */   private static final long LIMITPERSEC_MAX = 5000L;
/*     */   private static String project;
/*     */   private static String application;
/*     */   private static long bizLogLimitPerSec;
/*     */   private static long errorLogLimitPerSec;
/*     */   private static String environment;
/*     */   
/*     */   static {
/*  60 */     loadProperties();
/*     */   }
/*     */   
/*     */   private static void loadProperties() {
/*  64 */     Properties properties = ConfigLoader.getProperties();
/*     */     
/*  66 */     project = properties.getProperty("mos.ops.project");
/*     */     
/*  68 */     application = properties.getProperty("mos.ops.application");
/*     */     
/*  70 */     environment = properties.getProperty("mos.ops.environment");
/*     */     
/*  72 */     String bizLogLimitConfig = properties.getProperty("mos.ops.limitPerSec.bizLog");
/*  73 */     if (bizLogLimitConfig == null || "".equals(bizLogLimitConfig)) {
/*  74 */       bizLogLimitPerSec = 1000L;
/*     */     } else {
/*     */       try {
/*  77 */         bizLogLimitPerSec = Long.parseLong(bizLogLimitConfig);
/*  78 */         if (bizLogLimitPerSec > 5000L) {
/*  79 */           log.info("Biz log limit per second is set to {} which exceeds max valud, set to {}.", Long.valueOf(bizLogLimitPerSec), Long.valueOf(5000L));
/*  80 */           bizLogLimitPerSec = 5000L;
/*     */         } 
/*  82 */       } catch (NumberFormatException e) {
/*  83 */         log.error("Inside OpsConfig.loadProperties, bizLogLimitPerSec NumberFormatException occurs.", e);
/*  84 */         bizLogLimitPerSec = 1000L;
/*     */       } 
/*     */     } 
/*     */     
/*  88 */     String errorLogLimitConfig = properties.getProperty("mos.ops.limitPerSec.errorLog");
/*  89 */     if (errorLogLimitConfig == null || "".equals(errorLogLimitConfig)) {
/*  90 */       errorLogLimitPerSec = 1000L;
/*     */     } else {
/*     */       try {
/*  93 */         errorLogLimitPerSec = Long.parseLong(errorLogLimitConfig);
/*  94 */         if (errorLogLimitPerSec > 5000L) {
/*  95 */           log.info("Error log limit per second is set to {} which exceeds max valud, set to {}.", Long.valueOf(errorLogLimitPerSec), Long.valueOf(5000L));
/*  96 */           errorLogLimitPerSec = 5000L;
/*     */         } 
/*  98 */       } catch (NumberFormatException e) {
/*  99 */         log.error("Inside OpsConfig.loadProperties, errorLogLimitPerSec NumberFormatException occurs.", e);
/* 100 */         errorLogLimitPerSec = 1000L;
/*     */       } 
/*     */     } 
/*     */   }
/*     */   
/*     */   public static String getProject() {
/* 106 */     return project;
/*     */   }
/*     */   
/*     */   public static String getApplication() {
/* 110 */     return application;
/*     */   }
/*     */   
/*     */   public static long getBizLogLimitPerSec() {
/* 114 */     return bizLogLimitPerSec;
/*     */   }
/*     */   
/*     */   public static long getErrorLogLimitPerSec() {
/* 118 */     return errorLogLimitPerSec;
/*     */   }
/*     */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\lo\\util\OpsConfig.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */