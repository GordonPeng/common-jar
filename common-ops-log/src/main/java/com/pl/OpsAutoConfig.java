package com.pl;

import cn.hutool.extra.spring.EnableSpringUtil;
import com.pl.configuration.OpsProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.reactive.function.client.WebClient;


@Configuration
@ComponentScan
@EnableScheduling
@EnableAsync
@EnableConfigurationProperties({OpsProperties.class})
@PropertySource({"classpath:/ops/kafka.properties"})
@EnableSpringUtil
public class OpsAutoConfig {
    @Bean
    @ConditionalOnMissingBean
    public WebClient webClient() {
        /* 52 */
        return WebClient.builder()

/* 54 */.build();
    }
}


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\OpsAutoConfig.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */