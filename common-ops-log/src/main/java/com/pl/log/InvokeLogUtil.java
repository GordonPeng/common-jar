/*    */ package com.pl.log;
/*    */ 
/*    */ import java.util.Stack;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class InvokeLogUtil
/*    */ {
/* 14 */   private static ThreadLocal<Stack<MethodInvokeLog>> methodInvokeLog = new ThreadLocal<>();
/*    */ 
/*    */   
/*    */   public static void setMethodInvokeLog(MethodInvokeLog invokeLog) {
/* 18 */     Stack<MethodInvokeLog> stack = methodInvokeLog.get();
/* 19 */     if (stack == null) {
/* 20 */       stack = new Stack<>();
/* 21 */       methodInvokeLog.set(stack);
/*    */     } 
/* 23 */     stack.push(invokeLog);
/*    */   }
/*    */   
/*    */   public static MethodInvokeLog getMethodInvokeLog() {
/* 27 */     Stack<MethodInvokeLog> stack = methodInvokeLog.get();
/* 28 */     if (stack != null && !stack.isEmpty()) {
/* 29 */       return stack.peek();
/*    */     }
/* 31 */     return null;
/*    */   }
/*    */ 
/*    */   
/*    */   public static void removeMethodInvokeLog() {
/* 36 */     Stack<MethodInvokeLog> stack = methodInvokeLog.get();
/* 37 */     if (stack != null && !stack.isEmpty()) {
/* 38 */       stack.pop();
/*    */     }
/* 40 */     if (stack.isEmpty())
/* 41 */       methodInvokeLog.remove(); 
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\log\InvokeLogUtil.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */