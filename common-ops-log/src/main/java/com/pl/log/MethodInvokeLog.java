/*    */ package com.pl.log;
/*    */ 
/*    */ import cn.hutool.core.lang.UUID;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class MethodInvokeLog
/*    */ {
/*    */   private String traceId;
/*    */   private String methodId;
/*    */   private int level;
/*    */   private String parentId;
/*    */   
/*    */   public MethodInvokeLog(String method) {
/* 22 */     this.traceId = UUID.fastUUID().toString(true);
/* 23 */     this.methodId = method;
/* 24 */     this.level = 0;
/*    */   }
/*    */   
/*    */   public MethodInvokeLog(String traceId, String method, int level, String parentId) {
/* 28 */     this.traceId = traceId;
/* 29 */     this.methodId = method;
/* 30 */     this.level = level;
/* 31 */     this.parentId = parentId;
/*    */   }
/*    */   
/*    */   public String getTraceId() {
/* 35 */     return this.traceId;
/*    */   }
/*    */   
/*    */   public void setTraceId(String traceId) {
/* 39 */     this.traceId = traceId;
/*    */   }
/*    */   
/*    */   public String getMethodId() {
/* 43 */     return this.methodId;
/*    */   }
/*    */   
/*    */   public void setMethodId(String methodId) {
/* 47 */     this.methodId = methodId;
/*    */   }
/*    */   
/*    */   public int getLevel() {
/* 51 */     return this.level;
/*    */   }
/*    */   
/*    */   public void setLevel(int level) {
/* 55 */     this.level = level;
/*    */   }
/*    */   
/*    */   public String getParentId() {
/* 59 */     return this.parentId;
/*    */   }
/*    */   
/*    */   public void setParentId(String parentId) {
/* 63 */     this.parentId = parentId;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\log\MethodInvokeLog.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */