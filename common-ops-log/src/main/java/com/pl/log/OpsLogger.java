/*     */ package com.pl.log;
/*     */ 
/*     */ import cn.hutool.core.exceptions.ExceptionUtil;
/*     */ import cn.hutool.core.util.StrUtil;
/*     */ import java.util.Date;
/*     */ import com.pl.event.BizLogOpsModel;
import com.pl.limit.FlowLimiterManager;
import com.pl.util.Constant;
import com.pl.util.OpsConfig;
import com.pl.util.TimeUtil;
import org.slf4j.Logger;
/*     */ import org.slf4j.LoggerFactory;
/*     */ import org.slf4j.helpers.FormattingTuple;
/*     */ import org.slf4j.helpers.MessageFormatter;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class OpsLogger
/*     */ {
/*  45 */   private static Logger logger = LoggerFactory.getLogger(OpsLogger.class);
/*     */   
/*  47 */   private static String LINE_SEPARATOR = System.getProperty("line.separator");
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private static final String LOG_TYPE_INFO = "info";
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private static final String LOG_TYPE_ERROR = "error";
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private static final String LOG_SUBTYPE_DEFAULT = "default";
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void log(String logSubType, String module, String msgContent, String msgDetail) {
/*  85 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/*  86 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/*  87 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*  88 */     StringBuilder sb = new StringBuilder();
/*  89 */     sb.append(msgContent);
/*  90 */     if (StrUtil.isNotBlank(msgDetail)) {
/*  91 */       sb.append(LINE_SEPARATOR);
/*  92 */       sb.append(msgDetail);
/*     */     } 
/*  94 */     publishLogEvent(className, methodName, lineNumber, module, sb.toString(), "info", logSubType);
/*     */   }
/*     */   
/*     */   public static void logTraceId(String logSubType, String module, String msgContent, String traceId) {
/*     */     try {
/*  99 */       String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 100 */       String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 101 */       int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/* 102 */       StringBuilder sb = new StringBuilder();
/* 103 */       sb.append(msgContent);
/* 104 */       publishLogEventTraceId(className, methodName, lineNumber, module, sb.toString(), traceId, logSubType);
/* 105 */     } catch (Exception e) {
/* 106 */       logger.error("logTraceId exception", e);
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   private static void publishLogEvent(String className, String methodName, int line, String module, String msgContent, String logType, String logSubType) {
/* 128 */     if (FlowLimiterManager.grabToken("log.biz.limiter", OpsConfig.getBizLogLimitPerSec())) {
/* 129 */       BizLogOpsModel event = new BizLogOpsModel(className, methodName, line, logType, logSubType, module, msgContent, new Date());
/*     */       
/* 131 */       Constant.GLOBAL_BIZ_LOG_MAP.put(TimeUtil.currentTimeMinuteStr(), event);
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   private static void publishLogEventTraceId(String className, String methodName, int line, String module, String msgContent, String traceId, String logSubType) {
/* 137 */     if (FlowLimiterManager.grabToken("log.biz.limiter", OpsConfig.getBizLogLimitPerSec())) {
/* 138 */       if (StrUtil.isBlank(traceId)) {
/* 139 */         MethodInvokeLog currentInvokeLog = InvokeLogUtil.getMethodInvokeLog();
/* 140 */         if (currentInvokeLog != null) {
/* 141 */           traceId = currentInvokeLog.getTraceId();
/*     */         }
/*     */       } 
/* 144 */       BizLogOpsModel event = new BizLogOpsModel(className, methodName, line, logSubType, module, msgContent, new Date(), traceId);
/*     */       
/* 146 */       Constant.GLOBAL_BIZ_LOG_MAP.put(TimeUtil.currentTimeMinuteStr(), event);
/*     */     } else {
/* 148 */       logger.error("Discard Log Business :{}", msgContent);
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   private static void publishLogEvent(String className, String methodName, int line, String msgContent, String logType, String logSubType) {
/* 154 */     if (FlowLimiterManager.grabToken("log.biz.limiter", OpsConfig.getBizLogLimitPerSec())) {
/* 155 */       BizLogOpsModel event = new BizLogOpsModel(className, methodName, line, logType, logSubType, null, msgContent, new Date());
/*     */       
/* 157 */       Constant.GLOBAL_BIZ_LOG_MAP.put(TimeUtil.currentTimeMinuteStr(), event);
/*     */     } 
/*     */   }
/*     */ 
/*     */   
/*     */   private static void publishLogEvent(String className, String methodName, int line, String msgContent, String logType) {
/* 163 */     if (FlowLimiterManager.grabToken("log.biz.limiter", OpsConfig.getBizLogLimitPerSec())) {
/* 164 */       BizLogOpsModel event = new BizLogOpsModel(className, methodName, line, logType, null, null, msgContent, new Date());
/*     */       
/* 166 */       Constant.GLOBAL_BIZ_LOG_MAP.put(TimeUtil.currentTimeMinuteStr(), event);
/*     */       
/* 168 */       if ("info".equals(logType)) {
/* 169 */         logger.info(msgContent);
/* 170 */       } else if ("error".equals(logType)) {
/* 171 */         logger.error(msgContent);
/*     */       } else {
/* 173 */         logger.info(msgContent);
/*     */       } 
/*     */     } 
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void info(String msg) {
/* 185 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 186 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 187 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/* 188 */     publishLogEvent(className, methodName, lineNumber, msg, "info");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void info(String format, Object arg) {
/* 202 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 203 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 204 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 206 */     String msg = getFormatMessage(format, new Object[] { arg });
/* 207 */     publishLogEvent(className, methodName, lineNumber, msg, "info");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void info(String format, Object arg1, Object arg2) {
/* 222 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 223 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 224 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 226 */     String msg = getFormatMessage(format, new Object[] { arg1, arg2 });
/* 227 */     publishLogEvent(className, methodName, lineNumber, msg, "info");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void info(String format, Object... arguments) {
/* 245 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 246 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 247 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 249 */     String msg = getFormatMessage(format, arguments);
/* 250 */     publishLogEvent(className, methodName, lineNumber, msg, "info");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void info(String msg, Throwable t) {
/* 261 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 262 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 263 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 265 */     String formatMessage = getFormatMessage(msg, new Object[] { t });
/* 266 */     publishLogEvent(className, methodName, lineNumber, formatMessage, "info");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void infoSubType(String logSubType, String msg) {
/* 275 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 276 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 277 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/* 278 */     publishLogEvent(className, methodName, lineNumber, msg, "info", logSubType);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void infoSubType(String logSubType, String format, Object arg) {
/* 288 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 289 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 290 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 292 */     String msg = getFormatMessage(format, new Object[] { arg });
/* 293 */     publishLogEvent(className, methodName, lineNumber, msg, "info", logSubType);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void infoSubType(String logSubType, String format, Object arg1, Object arg2) {
/* 304 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 305 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 306 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 308 */     String msg = getFormatMessage(format, new Object[] { arg1, arg2 });
/* 309 */     publishLogEvent(className, methodName, lineNumber, msg, "info", logSubType);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void infoSubType(String logSubType, String format, Object... arguments) {
/* 319 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 320 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 321 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 323 */     String msg = getFormatMessage(format, arguments);
/* 324 */     publishLogEvent(className, methodName, lineNumber, msg, "info", logSubType);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void infoSubType(String logSubType, String msg, Throwable t) {
/* 334 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 335 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 336 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 338 */     String formatMessage = getFormatMessage(msg, new Object[] { t });
/* 339 */     publishLogEvent(className, methodName, lineNumber, formatMessage, "info", logSubType);
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void error(String msg) {
/* 348 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 349 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 350 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/* 351 */     publishLogEvent(className, methodName, lineNumber, msg, "info");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void error(String format, Object arg) {
/* 365 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 366 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 367 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 369 */     String msg = getFormatMessage(format, new Object[] { arg });
/* 370 */     publishLogEvent(className, methodName, lineNumber, msg, "error");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void error(String format, Object arg1, Object arg2) {
/* 385 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 386 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 387 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 389 */     String msg = getFormatMessage(format, new Object[] { arg1, arg2 });
/* 390 */     publishLogEvent(className, methodName, lineNumber, msg, "error");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void error(String format, Object... arguments) {
/* 408 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 409 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 410 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 412 */     String msg = getFormatMessage(format, arguments);
/* 413 */     publishLogEvent(className, methodName, lineNumber, msg, "error");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void error(String msg, Throwable t) {
/* 424 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 425 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 426 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 428 */     String formatMessage = getFormatMessage(msg, new Object[] { t });
/* 429 */     publishLogEvent(className, methodName, lineNumber, formatMessage, "error");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void errorSubType(String logSubType, String msg) {
/* 440 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 441 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 442 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/* 443 */     publishLogEvent(className, methodName, lineNumber, msg, "info");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void errorSubType(String logSubType, String format, Object arg) {
/* 459 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 460 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 461 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 463 */     String msg = getFormatMessage(format, new Object[] { arg });
/* 464 */     publishLogEvent(className, methodName, lineNumber, msg, "error");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void errorSubTYpe(String logSubType, String format, Object arg1, Object arg2) {
/* 481 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 482 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 483 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 485 */     String msg = getFormatMessage(format, new Object[] { arg1, arg2 });
/* 486 */     publishLogEvent(className, methodName, lineNumber, msg, "error");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void errorSubType(String logSubType, String format, Object... arguments) {
/* 506 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 507 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 508 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 510 */     String msg = getFormatMessage(format, arguments);
/* 511 */     publishLogEvent(className, methodName, lineNumber, msg, "error");
/*     */   }
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */   
/*     */   public static void errorSubType(String logSubType, String msg, Throwable t) {
/* 525 */     String className = Thread.currentThread().getStackTrace()[2].getClassName();
/* 526 */     String methodName = Thread.currentThread().getStackTrace()[2].getMethodName();
/* 527 */     int lineNumber = Thread.currentThread().getStackTrace()[2].getLineNumber();
/*     */     
/* 529 */     String formatMessage = getFormatMessage(msg, new Object[] { t });
/* 530 */     publishLogEvent(className, methodName, lineNumber, formatMessage, "error");
/*     */   }
/*     */   
/*     */   private static String getFormatMessage(String format, Object... arguments) {
/*     */     FormattingTuple formattingTuple;
/* 535 */     if (arguments.length == 1) {
/* 536 */       formattingTuple = MessageFormatter.format(format, arguments[0]);
/* 537 */     } else if (arguments.length == 2) {
/* 538 */       formattingTuple = MessageFormatter.format(format, arguments[0], arguments[1]);
/*     */     } else {
/* 540 */       formattingTuple = MessageFormatter.arrayFormat(format, arguments);
/*     */     } 
/* 542 */     StringBuilder sb = new StringBuilder();
/* 543 */     sb.append(formattingTuple.getMessage());
/* 544 */     if (formattingTuple.getThrowable() != null) {
/* 545 */       sb.append(LINE_SEPARATOR);
/* 546 */       sb.append(ExceptionUtil.stacktraceToString(formattingTuple.getThrowable()));
/*     */     } 
/* 548 */     logger.info(sb.toString());
/* 549 */     return sb.toString();
/*     */   }
/*     */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\log\OpsLogger.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */