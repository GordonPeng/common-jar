/*    */ package com.pl.log;
/*    */ 
/*    */ import cn.hutool.extra.spring.SpringUtil;
/*    */ import com.fasterxml.jackson.core.JsonProcessingException;
import com.pl.OpsJob;
import com.pl.client.IOpsClient;
import com.pl.event.ExtApiLogOpsEvent;
/*    */ import java.util.Date;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class ExtApiLogger
/*    */ {
/*    */   private static String project;
/*    */   private static String application;
/*    */   private static IOpsClient opsClient;
/*    */   
/*    */   public static void log(String apiId, String uri, String requestHeader, String requestParams, String responseBody, String responseCode, Date invokeTime, boolean isSuccess) throws JsonProcessingException {
/* 29 */     ExtApiLogOpsEvent event = new ExtApiLogOpsEvent(getProject(), getApplication(), apiId, uri, requestHeader, requestParams, responseBody, responseCode, invokeTime, isSuccess);
/*    */ 
/*    */     
/* 32 */     IOpsClient opsClient = getOpsClient();
/* 33 */     opsClient.sendExtApiLogOpsEvent(event);
/*    */   }
/*    */ 
/*    */   
/*    */   public static void log(String apiId, String uri, String requestHeader, String requestParams, Date invokeTime, String errorMsg) throws JsonProcessingException {
/* 38 */     ExtApiLogOpsEvent event = new ExtApiLogOpsEvent(getProject(), getApplication(), apiId, uri, requestHeader, requestParams, invokeTime, errorMsg);
/*    */ 
/*    */     
/* 41 */     IOpsClient opsClient = getOpsClient();
/* 42 */     opsClient.sendExtApiLogOpsEvent(event);
/*    */   }
/*    */ 
/*    */ 
/*    */   
/*    */   public static void log(String apiId, String thirdPlatform, String apiDesc, String uri, String requestHeader, String requestParams, String responseBody, String responseCode, Date invokeTime, boolean isSuccess, String errorMsg) throws JsonProcessingException {
/* 48 */     ExtApiLogOpsEvent event = new ExtApiLogOpsEvent(getProject(), getApplication(), apiId, thirdPlatform, apiDesc, uri, requestHeader, requestParams, responseBody, responseCode, invokeTime, isSuccess, errorMsg);
/*    */ 
/*    */     
/* 51 */     IOpsClient opsClient = getOpsClient();
/* 52 */     opsClient.sendExtApiLogOpsEvent(event);
/*    */   }
/*    */ 
/*    */   
/*    */   private static IOpsClient getOpsClient() {
/* 57 */     if (opsClient == null) {
/* 58 */       opsClient = (IOpsClient)SpringUtil.getBean(IOpsClient.class);
/*    */     }
/*    */     
/* 61 */     return opsClient;
/*    */   }
/*    */ 
/*    */   
/*    */   private static String getProject() {
/* 66 */     if (project == null) {
/* 67 */       OpsJob opsJob = (OpsJob)SpringUtil.getBean(OpsJob.class);
/* 68 */       project = opsJob.getProject();
/*    */     } 
/*    */     
/* 71 */     return project;
/*    */   }
/*    */   
/*    */   private static String getApplication() {
/* 75 */     if (application == null) {
/* 76 */       OpsJob opsJob = (OpsJob)SpringUtil.getBean(OpsJob.class);
/* 77 */       application = opsJob.getApplication();
/*    */     } 
/*    */     
/* 80 */     return application;
/*    */   }
/*    */ }


/* Location:              E:\code\common\cmsr-common-ops-log\1.0.0-SNAPSHOT\cmsr-common-ops-log-1.0.0-SNAPSHOT.jar!\com\cmsr\common\ops\log\log\ExtApiLogger.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       1.1.3
 */