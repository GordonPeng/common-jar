package com.pl.redis.service;

import com.pl.redis.RedisKeys;
import com.pl.redis.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * ----------------------------------------------------------------------------
 * Date            Author           Version        Comments
 * 2022/1/27 11:06  pengli         1.0       Initial Version
 */
public abstract class AbstractCacheService {

    protected static final long CACHE_EXPIRE_TIMEOUT = 24 * 60 * 60L;

    @Autowired
    protected RedisKeys redisKeys;

    @Autowired
    protected RedisUtil redisUtil;
}

