package com.pl.redis;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Date            Author           Version
 * 2022/1/27 14:10  pengli         1.0
 */
@ConfigurationProperties(prefix = "common.redis.cache")
@Data
public class CacheConfigProperties {

    /** redis缓存ttl, 单位: 秒, 默认: 30秒 */
    private int ttlInSec = 30;

    /**
     * entity id 和 serial缓存有效期，默认：24小时
     */
    private Long entityIdTtlInSec;

}