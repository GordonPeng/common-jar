package com.pl.redis.cache;

import lombok.Data;

/**
 * Date            Author           Version
 * 2022/1/27 15:19  pengli         1.0
 */
@Data
public class EntityIdCacheModel {
    /**
     * 数据库的主键ID
     */
    private Long id;

    /**
     * 数据字段关联serial
     */
    private String serial;

    public EntityIdCacheModel() {
    }

    public EntityIdCacheModel(Long id, String serial) {
        this.id = id;
        this.serial = serial;
    }
}
