package com.pl.redis.cache;

import com.pl.redis.CacheConfigProperties;
import com.pl.redis.RedisKeys;
import com.pl.redis.service.AbstractCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

/**
 * Date            Author           Version
 * 2022/1/27 15:18  pengli         1.0
 */
@Service
public class EntityIdCacheService extends AbstractCacheService {

    private static final String KEY_ENTITY_ID = "entity_id" + RedisKeys.KEY_SEPARATOR;

    /**
     * entity id缓存35天
     */
    private static final long ENTITY_ID_TTL = 35 * 24 * 3600;

    private final long entityIdTTL;

    @Autowired
    public EntityIdCacheService(CacheConfigProperties cacheConfigProperties) {
        if (cacheConfigProperties.getEntityIdTtlInSec() != null) {
            entityIdTTL = cacheConfigProperties.getEntityIdTtlInSec();
        } else {
            entityIdTTL = ENTITY_ID_TTL;
        }
    }

    /**
     * 数据库主键ID缓存
     */
    private Map<String, EntityIdCacheModel> entityIdMap = new ConcurrentHashMap<>();

    public void setEntityId(String suffixKey, EntityIdCacheModel idCache) {
        if (entityIdMap.containsKey(suffixKey)) {
            return;
        }
        entityIdMap.put(suffixKey, idCache);
        redisUtil.setWithPrefixWithExpire(redisKeys.getKey(KEY_ENTITY_ID), suffixKey, idCache, entityIdTTL);
    }

    public EntityIdCacheModel getEntityId(String suffixKey, Function<String, EntityIdCacheModel> idLoad) {
        if (entityIdMap.containsKey(suffixKey)) {
            return entityIdMap.get(suffixKey);
        } else {
            EntityIdCacheModel idCache = (EntityIdCacheModel) redisUtil.getWithPrefix(redisKeys.getKey(KEY_ENTITY_ID), suffixKey);
            if (idCache == null) {
                if (idLoad != null) {
                    idCache = idLoad.apply(suffixKey);
                    if (idCache != null) {
                        setEntityId(suffixKey, idCache);
                    }
                }
            } else {
                entityIdMap.put(suffixKey, idCache);
            }
            return idCache;
        }
    }


    /**
     * 每个月，清一次 entity-id map，否则会内存泄露
     */
    @Async
    @Scheduled(cron = "43 0 0 1 * ?")
    public void refreshEntityIdMap() {
        synchronized (this) {
            entityIdMap.clear();
        }
    }


}

