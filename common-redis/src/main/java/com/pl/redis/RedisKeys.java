package com.pl.redis;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Date            Author           Version        Comments
 * 2022/1/27 11:08  pengli         1.0       Initial Version
 */
@Component("commonRedisKeys")
@Getter
public class RedisKeys {

    public static String KEY_SEPARATOR = ":";

    @Value("${project:cmsr}")
    private String project;

    @Value("${environment:local}")
    private String environment;

    public String getKey(String suffix) {
        return project + KEY_SEPARATOR + environment + KEY_SEPARATOR + suffix;
    }
}
